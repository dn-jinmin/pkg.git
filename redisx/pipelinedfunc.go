/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package redisx

import (
	"context"
	red "github.com/go-redis/redis/v8"
	"github.com/zeromicro/go-zero/core/stores/redis"
)

// Zadd
func PipelinedZaddFunc(key string, score float64, member interface{}) PipelinedFunc {
	return PipelinedZaddsFunc(key, &redis.Z{
		score,
		member,
	})
}

// Zadds
//
//	PipliedZrangeFunc
func PipelinedZaddsFunc(key string, z ...*redis.Z) PipelinedFunc {
	return func(pipe red.Pipeliner, ctx context.Context) {
		pipe.ZAdd(ctx, key, z...)
	}
}

// Zrem
func PipelinedZRemFunc(key string, members ...interface{}) PipelinedFunc {
	return func(pipe red.Pipeliner, ctx context.Context) {
		pipe.ZRem(ctx, key, members...)
	}
}

func PipelinedZrangeFunc(key string, start, end int64) PipelinedFunc {
	return func(pipe red.Pipeliner, ctx context.Context) {
		pipe.ZRange(ctx, key, start, end)
	}
}

// ZREMRANGEBYSCORE
func PipelinedZremrangebyscoreFunc(key, min, max string) PipelinedFunc {
	return func(pipe red.Pipeliner, ctx context.Context) {
		pipe.ZRemRangeByScore(ctx, key, min, max)
	}
}

func PipelinedZrangebyscoreFunc(key string, opt *red.ZRangeBy) PipelinedFunc {
	return func(pipe red.Pipeliner, ctx context.Context) {
		pipe.ZRangeByScore(ctx, key, opt)
	}
}
