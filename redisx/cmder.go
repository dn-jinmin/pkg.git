/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */

package redisx

import (
	"github.com/go-redis/redis"
	"regexp"
	"strings"
)

func CmdStr(cmd redis.Cmder) string {
	str := strings.Split(cmd.String(), " ")
	return str[len(str)-1]
}

func CmdZset(cmd redis.Cmder) []string {
	re := regexp.MustCompile(`\[(.*?)\]`)
	rs := re.FindStringSubmatch(cmd.String())
	return strings.Split(rs[1], " ")
}
