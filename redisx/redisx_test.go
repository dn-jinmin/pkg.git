/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package redisx

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/zeromicro/go-zero/core/stores/redis"
)

func getRedisx() *Redisx {
	var RedisxConf = []NodeConf{
		{
			RedisConf: redis.RedisConf{
				Host: "127.0.0.1:6379",
				Type: "node",
			},
			Weight: 100,
		},
	}
	return New(RedisxConf)
}

func TestRedisx_PipelinedCtx(t *testing.T) {
	ctx := context.Background()
	type args struct {
		fn func(redis.Pipeliner) error
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"1", args{func(pipeliner redis.Pipeliner) error {
			pipeliner.HSet(ctx, "test:user:1", "name", "hset")
			pipeliner.HSet(ctx, "test:user:2", "name", "hset")
			return nil
		}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rx := getRedisx()
			if err := rx.PipelinedCtx(context.Background(), tt.args.fn); (err != nil) != tt.wantErr {
				t.Errorf("PipelinedCtx() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRedisx_Zadds(t *testing.T) {
	type args struct {
		key string
		ps  []redis.Pair
	}
	now := time.Now()
	tests := []struct {
		name    string
		args    args
		want    int64
		wantErr bool
	}{
		{"1", args{
			key: "test:zadds",
			ps: []redis.Pair{
				{
					".net",
					18,
				}, {
					"go",
					20,
				}, {
					"java",
					32,
				},
			},
		}, 3, false},
		{"2", args{
			key: "test:zadds:time",
			ps: []redis.Pair{
				{
					"go-1",
					now.UnixMilli(),
				}, {
					"go-2",
					now.Add(30 * time.Minute).UnixMilli(),
				}, {
					"go-3",
					now.Add(30 * time.Minute).UnixMilli(),
				}, {
					"go-4",
					now.Add(60 * time.Minute).UnixMilli(),
				}, {
					"go-5",
					now.Add(90 * time.Minute).UnixMilli(),
				}, {
					"go-6",
					now.Add(120 * time.Minute).UnixMilli(),
				},
			},
		}, 5, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rx := getRedisx()
			got, err := rx.Zadds(tt.args.key, tt.args.ps...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Zadds() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Zadds() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedisx_Zrange(t *testing.T) {
	type args struct {
		key   string
		start int64
		stop  int64
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{"1", args{
			key:   "test:zadds",
			start: 0,
			stop:  -1,
		}, []string{".net", "php", "go", "java"}, false},
		{"2", args{
			key:   "test:zadds:time",
			start: -1,
			stop:  -1,
		}, []string{".net", "php", "go", "java"}, false},
		{"3", args{
			key:   "test:zadds:time",
			start: -1,
			stop:  0,
		}, []string{".net", "php", "go", "java"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rx := getRedisx()
			got, err := rx.Zrange(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Zrange() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Zrange() got = %v, want %v", got, tt.want)
			}
			t.Log(got)
		})
	}
}
