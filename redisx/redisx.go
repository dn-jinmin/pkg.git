package redisx

import (
	"context"
	"fmt"
	red "github.com/go-redis/redis/v8"
	"github.com/zeromicro/go-zero/core/hash"
	"github.com/zeromicro/go-zero/core/stores/redis"
	"log"
)

const (
	defaultDatabase = 0
	maxRetries      = 3
	idleConns       = 8
)

type Redisx struct {
	Type        string
	redisNode   *redis.Redis
	dispatcher  *hash.ConsistentHash
	errNotFound error

	cfg ClusterConf
}

func New(c ClusterConf) *Redisx {
	var r Redisx
	if len(c) == 0 || TotalWeights(c) <= 0 {
		log.Fatal("no cache nodes")
	}
	r.cfg = c
	// 单个节点
	if len(c) == 1 {
		r.redisNode = c[0].NewRedis()
		r.Type = redis.NodeType

		return &r
	}
	r.Type = redis.ClusterType

	dispatcher := hash.NewConsistentHash()
	for _, node := range c {
		cn := node.NewRedis()
		dispatcher.AddWithWeight(cn, node.Weight)
	}

	r.dispatcher = dispatcher
	return &r
}

func (rx *Redisx) Del(keys ...string) (int, error) {
	return rx.DelCtx(context.Background(), keys...)
}

// DelCtx deletes keys.
func (rx *Redisx) DelCtx(ctx context.Context, keys ...string) (val int, err error) {
	s, err := rx.getRedis(keys[0])
	if err != nil {
		return 0, err
	}
	return s.DelCtx(ctx, keys...)
}

func (rx *Redisx) Set(key, value string) error {
	s, err := rx.getRedis(key)
	if err != nil {
		return err
	}
	return s.SetCtx(context.Background(), key, value)
}

func (rx *Redisx) Get(key string) (string, error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return "", err
	}
	return s.GetCtx(context.Background(), key)
}

func (rx *Redisx) Exists(key string) (bool, error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return false, err
	}
	return s.ExistsCtx(context.Background(), key)
}

// Hexists is the implementation of redis hexists command.
func (rx *Redisx) Hexists(key, field string) (bool, error) {
	return rx.HexistsCtx(context.Background(), key, field)
}

// HexistsCtx is the implementation of redis hexists command.
func (rx *Redisx) HexistsCtx(ctx context.Context, key, field string) (val bool, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return false, err
	}
	return s.HexistsCtx(ctx, key, field)
}

// Hdel is the implementation of redis hdel command.
func (rx *Redisx) Hdel(key string, fields ...string) (bool, error) {
	return rx.HdelCtx(context.Background(), key, fields...)
}

// HdelCtx is the implementation of redis hdel command.
func (rx *Redisx) HdelCtx(ctx context.Context, key string, fields ...string) (val bool, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return false, err
	}
	return s.HdelCtx(context.Background(), key, fields...)
}

// Hget is the implementation of redis hget command.
func (rx *Redisx) Hget(key, field string) (string, error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return "", err
	}
	return s.HgetCtx(context.Background(), key, field)
}

// HgetCtx is the implementation of redis hget command.
func (rx *Redisx) HgetCtx(ctx context.Context, key, field string) (string, error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return "", err
	}
	return s.HgetCtx(ctx, key, field)
}

// 设置hash值单个field
func (rx *Redisx) Hset(key, field, value string) error {
	s, err := rx.getRedis(key)
	if err != nil {
		return err
	}
	return s.HsetCtx(context.Background(), key, field, value)
}

// 设置hash值多个field
func (rx *Redisx) Hmset(key string, fieldsAndValues map[string]string) error {
	s, err := rx.getRedis(key)
	if err != nil {
		return err
	}
	return s.HmsetCtx(context.Background(), key, fieldsAndValues)
}

// 设置hash值多个field，协程下运用
func (rx *Redisx) HmsetCtx(ctx context.Context, key string, fieldsAndValues map[string]string) error {
	s, err := rx.getRedis(key)
	if err != nil {
		return err
	}
	return s.HmsetCtx(ctx, key, fieldsAndValues)
}

func (rx *Redisx) Sadd(key string, values ...interface{}) (int, error) {
	return rx.SaddCtx(context.Background(), key, values...)
}

// SaddCtx is the implementation of redis sadd command.
func (rx *Redisx) SaddCtx(ctx context.Context, key string, values ...interface{}) (val int, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}
	return s.SaddCtx(ctx, key, values...)
}

// Smembers is the implementation of redis smembers command.
func (rx *Redisx) Smembers(key string) ([]string, error) {
	return rx.SmembersCtx(context.Background(), key)
}

// SmembersCtx is the implementation of redis smembers command.
func (rx *Redisx) SmembersCtx(ctx context.Context, key string) (val []string, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}
	return s.SmembersCtx(context.Background(), key)
}

// Srem is the implementation of redis srem command.
func (rx *Redisx) Srem(key string, values ...interface{}) (int, error) {
	return rx.SremCtx(context.Background(), key, values...)
}

// SremCtx is the implementation of redis srem command.
func (rx *Redisx) SremCtx(ctx context.Context, key string, values ...interface{}) (val int, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}
	return s.SremCtx(ctx, key, values...)
}

// Pipelined lets fn execute pipelined commands.
func (rx *Redisx) Pipelined(fn func(redis.Pipeliner) error) error {
	return rx.PipelinedCtx(context.Background(), fn)
}

// 管道的理解 https://juejin.cn/post/6844904127001001991
// PipelinedCtx lets fn execute pipelined commands.
// Results need to be retrieved by calling Pipeline.Exec()
func (rx *Redisx) PipelinedCtx(ctx context.Context, fn func(redis.Pipeliner) error) error {
	// redis的集群获取时基于hash方式获取，通过key的运算
	s, err := rx.getRedis("pipelined")
	if err != nil {
		return err
	}
	return s.PipelinedCtx(ctx, fn)
}

// Zadd is the implementation of redis zadd command.
func (rx *Redisx) Zadd(key string, score int64, value string) (bool, error) {
	return rx.ZaddCtx(context.Background(), key, score, value)
}

// ZaddFloat is the implementation of redis zadd command.
func (rx *Redisx) ZaddFloat(key string, score float64, value string) (bool, error) {
	return rx.ZaddFloatCtx(context.Background(), key, score, value)
}

// ZaddCtx is the implementation of redis zadd command.
func (rx *Redisx) ZaddCtx(ctx context.Context, key string, score int64, value string) (
	val bool, err error) {
	return rx.ZaddFloatCtx(ctx, key, float64(score), value)
}

// ZaddFloatCtx is the implementation of redis zadd command.
func (rx *Redisx) ZaddFloatCtx(ctx context.Context, key string, score float64, value string) (
	val bool, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return false, err
	}
	return s.ZaddFloatCtx(ctx, key, float64(score), value)
}

// Zadds is the implementation of redis zadds command.
func (rx *Redisx) Zadds(key string, ps ...redis.Pair) (int64, error) {
	return rx.ZaddsCtx(context.Background(), key, ps...)
}

// ZaddsCtx is the implementation of redis zadds command.
func (rx *Redisx) ZaddsCtx(ctx context.Context, key string, ps ...redis.Pair) (val int64, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}

	return s.ZaddsCtx(ctx, key, ps...)
}

// Zcard is the implementation of redis zcard command.
func (rx *Redisx) Zcard(key string) (int, error) {
	return rx.ZcardCtx(context.Background(), key)
}

// ZcardCtx is the implementation of redis zcard command.
func (rx *Redisx) ZcardCtx(ctx context.Context, key string) (val int, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}

	return s.ZcardCtx(ctx, key)
}

// Zcount is the implementation of redis zcount command.
func (rx *Redisx) Zcount(key string, start, stop int64) (int, error) {
	return rx.ZcountCtx(context.Background(), key, start, stop)
}

// ZcountCtx is the implementation of redis zcount command.
func (rx *Redisx) ZcountCtx(ctx context.Context, key string, start, stop int64) (val int, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}
	return s.ZcountCtx(ctx, key, start, stop)
}

// Zincrby is the implementation of redis zincrby command.
func (rx *Redisx) Zincrby(key string, increment int64, field string) (int64, error) {
	return rx.ZincrbyCtx(context.Background(), key, increment, field)
}

// ZincrbyCtx is the implementation of redis zincrby command.
func (rx *Redisx) ZincrbyCtx(ctx context.Context, key string, increment int64, field string) (
	val int64, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}
	return s.ZincrbyCtx(ctx, key, increment, field)
}

// Zscore is the implementation of redis zscore command.
func (rx *Redisx) Zscore(key, value string) (int64, error) {
	return rx.ZscoreCtx(context.Background(), key, value)
}

// ZscoreCtx is the implementation of redis zscore command.
func (rx *Redisx) ZscoreCtx(ctx context.Context, key, value string) (val int64, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}

	return s.ZscoreCtx(ctx, key, value)
}

// Zrank is the implementation of redis zrank command.
func (rx *Redisx) Zrank(key, field string) (int64, error) {
	return rx.ZrankCtx(context.Background(), key, field)
}

// ZrankCtx is the implementation of redis zrank command.
func (rx *Redisx) ZrankCtx(ctx context.Context, key, field string) (val int64, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}

	return s.ZrankCtx(ctx, key, field)
}

// Zrem is the implementation of redis zrem command.
func (rx *Redisx) Zrem(key string, values ...interface{}) (int, error) {
	return rx.ZremCtx(context.Background(), key, values...)
}

// ZremCtx is the implementation of redis zrem command.
func (rx *Redisx) ZremCtx(ctx context.Context, key string, values ...interface{}) (val int, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}
	return s.ZremCtx(ctx, key, values...)
}

// Zremrangebyscore is the implementation of redis zremrangebyscore command.
func (rx *Redisx) Zremrangebyscore(key string, start, stop int64) (int, error) {
	return rx.ZremrangebyscoreCtx(context.Background(), key, start, stop)
}

// ZremrangebyscoreCtx is the implementation of redis zremrangebyscore command.
func (rx *Redisx) ZremrangebyscoreCtx(ctx context.Context, key string, start, stop int64) (
	val int, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}

	return s.ZremrangebyscoreCtx(ctx, key, start, stop)
}

// Zremrangebyrank is the implementation of redis zremrangebyrank command.
func (rx *Redisx) Zremrangebyrank(key string, start, stop int64) (int, error) {
	return rx.ZremrangebyrankCtx(context.Background(), key, start, stop)
}

// ZremrangebyrankCtx is the implementation of redis zremrangebyrank command.
func (rx *Redisx) ZremrangebyrankCtx(ctx context.Context, key string, start, stop int64) (
	val int, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}

	return s.ZremrangebyrankCtx(ctx, key, start, stop)
}

// Zrange is the implementation of redis zrange command.
func (rx *Redisx) Zrange(key string, start, stop int64) ([]string, error) {
	return rx.ZrangeCtx(context.Background(), key, start, stop)
}

// ZrangeCtx is the implementation of redis zrange command.
func (rx *Redisx) ZrangeCtx(ctx context.Context, key string, start, stop int64) (
	val []string, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZrangeCtx(ctx, key, start, stop)
}

// ZrangeWithScores is the implementation of redis zrange command with scores.
func (rx *Redisx) ZrangeWithScores(key string, start, stop int64) ([]redis.Pair, error) {
	return rx.ZrangeWithScoresCtx(context.Background(), key, start, stop)
}

// ZrangeWithScoresCtx is the implementation of redis zrange command with scores.
func (rx *Redisx) ZrangeWithScoresCtx(ctx context.Context, key string, start, stop int64) (
	val []redis.Pair, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZrangeWithScoresCtx(ctx, key, start, stop)
}

// ZRevRangeWithScores is the implementation of redis zrevrange command with scores.
func (rx *Redisx) ZRevRangeWithScores(key string, start, stop int64) ([]redis.Pair, error) {
	return rx.ZRevRangeWithScoresCtx(context.Background(), key, start, stop)
}

// ZRevRangeWithScoresCtx is the implementation of redis zrevrange command with scores.
func (rx *Redisx) ZRevRangeWithScoresCtx(ctx context.Context, key string, start, stop int64) (
	val []redis.Pair, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZRevRangeWithScoresCtx(ctx, key, start, stop)
}

// ZrangebyscoreWithScores is the implementation of redis zrangebyscore command with scores.
func (rx *Redisx) ZrangebyscoreWithScores(key string, start, stop int64) ([]redis.Pair, error) {
	return rx.ZrangebyscoreWithScoresCtx(context.Background(), key, start, stop)
}

// ZrangebyscoreWithScoresCtx is the implementation of redis zrangebyscore command with scores.
func (rx *Redisx) ZrangebyscoreWithScoresCtx(ctx context.Context, key string, start, stop int64) (
	val []redis.Pair, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZrangebyscoreWithScoresCtx(ctx, key, start, stop)
}

// ZrangebyscoreWithScoresAndLimit is the implementation of redis zrangebyscore command
// with scores and limit.
func (rx *Redisx) ZrangebyscoreWithScoresAndLimit(key string, start, stop int64,
	page, size int) ([]redis.Pair, error) {
	return rx.ZrangebyscoreWithScoresAndLimitCtx(context.Background(), key, start, stop, page, size)
}

// ZrangebyscoreWithScoresAndLimitCtx is the implementation of redis zrangebyscore command
// with scores and limit.
func (rx *Redisx) ZrangebyscoreWithScoresAndLimitCtx(ctx context.Context, key string, start,
	stop int64, page, size int) (val []redis.Pair, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZrangebyscoreWithScoresAndLimitCtx(ctx, key, start, stop, page, size)
}

// Zrevrange is the implementation of redis zrevrange command.
func (rx *Redisx) Zrevrange(key string, start, stop int64) ([]string, error) {
	return rx.ZrevrangeCtx(context.Background(), key, start, stop)
}

// ZrevrangeCtx is the implementation of redis zrevrange command.
func (rx *Redisx) ZrevrangeCtx(ctx context.Context, key string, start, stop int64) (
	val []string, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZrevrangeCtx(ctx, key, start, stop)
}

// ZrevrangebyscoreWithScores is the implementation of redis zrevrangebyscore command with scores.
func (rx *Redisx) ZrevrangebyscoreWithScores(key string, start, stop int64) ([]redis.Pair, error) {
	return rx.ZrevrangebyscoreWithScoresCtx(context.Background(), key, start, stop)
}

// ZrevrangebyscoreWithScoresCtx is the implementation of redis zrevrangebyscore command with scores.
func (rx *Redisx) ZrevrangebyscoreWithScoresCtx(ctx context.Context, key string, start, stop int64) (
	val []redis.Pair, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZrevrangebyscoreWithScoresCtx(ctx, key, start, stop)
}

// ZrevrangebyscoreWithScoresAndLimit is the implementation of redis zrevrangebyscore command
// with scores and limit.
func (rx *Redisx) ZrevrangebyscoreWithScoresAndLimit(key string, start, stop int64,
	page, size int) ([]redis.Pair, error) {
	return rx.ZrevrangebyscoreWithScoresAndLimitCtx(context.Background(), key, start, stop, page, size)
}

// ZrevrangebyscoreWithScoresAndLimitCtx is the implementation of redis zrevrangebyscore command
// with scores and limit.
func (rx *Redisx) ZrevrangebyscoreWithScoresAndLimitCtx(ctx context.Context, key string,
	start, stop int64, page, size int) (val []redis.Pair, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.ZrevrangebyscoreWithScoresAndLimitCtx(ctx, key, start, stop, page, size)
}

// Zrevrank is the implementation of redis zrevrank command.
func (rx *Redisx) Zrevrank(key, field string) (int64, error) {
	return rx.ZrevrankCtx(context.Background(), key, field)
}

// ZrevrankCtx is the implementation of redis zrevrank command.
func (rx *Redisx) ZrevrankCtx(ctx context.Context, key, field string) (val int64, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return 0, err
	}

	return s.ZrevrankCtx(ctx, key, field)
}

// Zunionstore is the implementation of redis zunionstore command.
func (rx *Redisx) Zunionstore(dest string, store *redis.ZStore) (int64, error) {
	return rx.ZunionstoreCtx(context.Background(), dest, store)
}

// ZunionstoreCtx is the implementation of redis zunionstore command.
func (rx *Redisx) ZunionstoreCtx(ctx context.Context, dest string, store *redis.ZStore) (val int64, err error) {
	s, err := rx.getRedis(dest)
	if err != nil {
		return 0, err
	}

	return s.ZunionstoreCtx(ctx, dest, store)
}

// Hgetall is the implementation of redis hgetall command.
func (rx *Redisx) Hgetall(key string) (map[string]string, error) {
	return rx.HgetallCtx(context.Background(), key)
}

// HgetallCtx is the implementation of redis hgetall command.
func (rx *Redisx) HgetallCtx(ctx context.Context, key string) (val map[string]string, err error) {
	s, err := rx.getRedis(key)
	if err != nil {
		return nil, err
	}

	return s.HgetallCtx(ctx, key)
}

func (rx *Redisx) getRedis(key ...string) (*redis.Redis, error) {
	switch rx.Type {
	case redis.NodeType:
		return rx.redisNode, nil
	case redis.ClusterType:
		r, ok := rx.dispatcher.Get(key[0])
		if !ok {
			return nil, rx.errNotFound
		}
		return r.(*redis.Redis), nil
	}
	return nil, fmt.Errorf("redis type '%s' is not supported", rx.Type)
}

func (rx *Redisx) Do(args ...interface{}) {

}
func (rx *Redisx) DoCtx(ctx context.Context, args ...interface{}) {

}

func (rx *Redisx) GetClient() (*red.Client, error) {
	r, err := rx.getRedis()
	if err != nil {
		return nil, err
	}
	cli := red.NewClient(&red.Options{
		Addr:         r.Addr,
		Password:     r.Pass,
		DB:           defaultDatabase,
		MaxRetries:   maxRetries,
		MinIdleConns: idleConns,
	})
	return cli, nil
}
