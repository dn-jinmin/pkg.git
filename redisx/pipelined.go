/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package redisx

import (
	"context"
	"github.com/go-redis/redis/v8"
)

type PipelinedFunc func(pipe redis.Pipeliner, ctx context.Context)

type Pipelined struct {
	redisx *Redisx
	funcs  []PipelinedFunc
}

func NewPipelined(redis *Redisx) *Pipelined {
	return &Pipelined{
		redisx: redis,
	}
}

func (p *Pipelined) AddFunc(f ...PipelinedFunc) {
	p.funcs = append(p.funcs, f...)
}

func (p *Pipelined) ExecFunc(ctx context.Context) error {
	err := p.redisx.Pipelined(func(pipeliner redis.Pipeliner) error {
		for _, pipelinedFunc := range p.funcs {
			pipelinedFunc(pipeliner, ctx)
		}
		return nil
	})
	p.funcs = nil
	return err
}

func (p *Pipelined) ExecFuncCmder(ctx context.Context) ([]redis.Cmder, error) {
	r, err := p.redisx.GetClient()
	if err != nil {
		return nil, err
	}

	cmds, err := r.Pipelined(ctx, func(pipeliner redis.Pipeliner) error {
		for _, pipelinedFunc := range p.funcs {
			pipelinedFunc(pipeliner, ctx)
		}
		return nil
	})
	p.funcs = nil
	return cmds, err
}
