package redisx

func TotalWeights(c []NodeConf) int {
	var weights int

	for _, node := range c {
		if node.Weight < 0 {
			node.Weight = 0
		}
		weights += node.Weight
	}

	return weights
}
