/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */

package redisx

import (
	"context"
	"github.com/go-redis/redis/v8"
	"regexp"
	"testing"
)

func TestNewPipelined_Zadd(t *testing.T) {
	type args struct {
		pipfunc []PipelinedFunc
	}
	tests := []struct {
		name string
		args args
	}{
		{"1", args{
			[]PipelinedFunc{
				PipelinedZaddFunc("test:zadd:1", 4, "ok3"),
				PipelinedZaddFunc("test:zadd:2", 5, "ok3"),
				PipelinedZaddFunc("test:zadd:3", 5, "ok3"),
				PipelinedZaddFunc("test:zadd:1", 9, "ok39"),
				PipelinedZaddFunc("test:zadd:2", 9, "ok39"),
				PipelinedZaddFunc("test:zadd:3", 9, "ok39"),
			},
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pip := NewPipelined(getRedisx())
			pip.AddFunc(tt.args.pipfunc...)
			pip.ExecFunc(context.Background())
		})
	}
}

func TestPipelined_Zrange(t *testing.T) {

	type args struct {
		pipfunc []PipelinedFunc
	}
	tests := []struct {
		name string
		args args
	}{
		{"1", args{
			[]PipelinedFunc{},
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := context.Background()
			p := NewPipelined(getRedisx())
			p.AddFunc(PipelinedZrangeFunc("test:zadd:1", 0, -1), PipelinedZrangeFunc("test:zadd:2", 0, -1), PipelinedZrangebyscoreFunc("test:zadd:2", &redis.ZRangeBy{
				Min:    "-inf",
				Max:    "+inf",
				Offset: 0,
				Count:  0,
			}))
			cmd, _ := p.ExecFuncCmder(ctx)
			for _, cmder := range cmd {
				c := cmder.(*redis.StringSliceCmd)
				t.Log(c.Val())
				t.Log(c.Args()[1])
			}
		})
	}
}

func TestCompile(t *testing.T) {
	re := regexp.MustCompile(`\[(.*?)\]`)
	str := "zrange test:zadd:1 0 -1: [ok3 ok ok39 ff]"
	t.Log(re.FindStringSubmatch(str)[1])

	s := `(tag)sss`

	rgs := regexp.MustCompile(`\((.*?)\)`)

	t.Log(rgs.FindStringSubmatch(s)[1])
	//t.Log(strings.Split("get test:str: test:str", " "))
}
