package result

import (
	"github.com/zeromicro/go-zero/core/logx"
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"
	"google.golang.org/grpc/status"

	"gitee.com/dn-jinmin/pkg/xerr"
)

func HttpResult(r *http.Request, w http.ResponseWriter, resp interface{}, err error) {
	if err == nil {
		r := SuccessWithData(resp)
		httpx.WriteJson(w, http.StatusOK, r)
		return
	}

	errcode := xerr.ServerCommonError
	errmsg := defaultErrorMessage

	causeErr := xerr.Cause(err)
	if e, ok := causeErr.(*xerr.CodeError); ok {
		logx.WithContext(r.Context()).Errorf("【API-ERR】 : %+v ", e)
		// 自定义错误
		errcode = e.GetErrCode()
		errmsg = e.GetErrMsg()
	} else {
		if gstatus, ok := status.FromError(causeErr); ok {

			// grpc发送错误
			grpcCode := uint32(gstatus.Code())
			if xerr.IsCodeErr(grpcCode) {
				errcode = grpcCode
				errmsg = gstatus.Message()
			}
		} else {
			errmsg = err.Error()
		}
	}

	httpx.WriteJson(w, http.StatusBadRequest, ErrorDetails(errcode, errmsg))

}
