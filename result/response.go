package result

import (
	"gitee.com/dn-jinmin/pkg/xerr"
)

const (
	msgNil = ""
	defaultErrorMessage = "服务器开小差啦，稍后再试"
)

type RespSuccess struct {
	Code uint32 		`json:"code"`
	Msg string			`json:"msg"`
	Data interface{}	`json:"data"`
}
func Success() *RespSuccess {
	return SuccessDetails(msgNil, nil)
}
func SuccessWithMsg(msg string) *RespSuccess {
	return SuccessDetails(msg, nil)
}
func SuccessWithData(data interface{}) *RespSuccess {
	return SuccessDetails(msgNil, data)
}
func SuccessDetails(msg string, data interface{}) *RespSuccess {
	return &RespSuccess{
		Code: xerr.OK,
		Msg:  msg,
		Data: data,
	}
}

type RespError struct {
	Code uint32 	`json:"code"`
	Msg string		`json:"msg"`
}

func Error() *RespError {
	return ErrorDetails(xerr.ServerCommonError, msgNil)
}
func ErrorWithCode(code uint32) *RespError {
	return ErrorDetails(code, msgNil)
}
func ErrorWithMsg(msg string) *RespError {
	return ErrorDetails(xerr.ServerCommonError, msg)
}
func ErrorWithErr(err error) *RespError {
	return ErrorDetails(xerr.ServerCommonError, err.Error())
}

func ErrorDetails(code uint32, msg string) *RespError {
	return &RespError{
		Code: code,
		Msg:  msg,
	}
}