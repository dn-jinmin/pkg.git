package cachex

import (
	"time"
)

// 缓存对象

type cacheMobile struct {
	key      string
	expire   time.Duration
	Contentx string
}

func NewCacheMobile(mobile string, expire int64, content string) *cacheMobile {
	return &cacheMobile{
		key:      prefixAPI + ":mobile:" + mobile,
		expire:   time.Duration(expire) * time.Second,
		Contentx: content,
	}
}
func (c *cacheMobile) Key() string {
	return c.key
}
func (c *cacheMobile) Expire() time.Duration {
	return c.expire
}
func (c *cacheMobile) Content() interface{} {
	return c.Contentx
}

type cacheTokenBlackList struct {
	key    string
	expire time.Duration
}

func NewCacheTokenBlackList(uid string, expire int64) *cacheTokenBlackList {
	return &cacheTokenBlackList{
		key:    prefixAPI + ":tokenBlackList:" + uid,
		expire: time.Duration(expire) * time.Second,
	}
}
func (c *cacheTokenBlackList) Key() string {
	return c.key
}
func (c *cacheTokenBlackList) Expire() time.Duration {
	return c.expire
}
