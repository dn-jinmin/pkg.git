package cachex

import (
	"context"
	"time"
)

const (
	prefixAPI = "api"
	prefixRPC = "rpc"
	prefixSYS = "sys"
)

var (
	ExpireNil     int64  = 0
	ContentIntNil int    = 0
	ContentStrNil string = ""
)

// 装饰缓存

type CacheObject interface {
	Key() string
	Expire() time.Duration
	Content() interface{}
}
type Cache interface {
	// Del deletes cached values with keys.
	Del(keys ...string) error
	// DelCtx deletes cached values with keys.
	DelCtx(ctx context.Context, keys ...string) error
	// Get gets the cache with key and fills into v.
	Get(key string, val any) error
	// GetCtx gets the cache with key and fills into v.
	GetCtx(ctx context.Context, key string, val any) error
	// IsNotFound checks if the given error is the defined errNotFound.
	IsNotFound(err error) bool
	// Set sets the cache with key and v, using c.expiry.
	Set(key string, val any) error
	// SetCtx sets the cache with key and v, using c.expiry.
	SetCtx(ctx context.Context, key string, val any) error
	// SetWithExpire sets the cache with key and v, using given expire.
	SetWithExpire(key string, val any, expire time.Duration) error
	// SetWithExpireCtx sets the cache with key and v, using given expire.
	SetWithExpireCtx(ctx context.Context, key string, val any, expire time.Duration) error
	// Take takes the result from cache first, if not found,
	// query from DB and set cache using c.expiry, then return the result.
	Take(val any, key string, query func(val any) error) error
	// TakeCtx takes the result from cache first, if not found,
	// query from DB and set cache using c.expiry, then return the result.
	TakeCtx(ctx context.Context, val any, key string, query func(val any) error) error
	// TakeWithExpire takes the result from cache first, if not found,
	// query from DB and set cache using given expire, then return the result.
	TakeWithExpire(val any, key string, query func(val any, expire time.Duration) error) error
	// TakeWithExpireCtx takes the result from cache first, if not found,
	// query from DB and set cache using given expire, then return the result.
	TakeWithExpireCtx(ctx context.Context, val any, key string,
		query func(val any, expire time.Duration) error) error
}

// 装饰缓存
// cacheObject
type cache struct {
	Cache Cache
}

func NewCache(c Cache) *cache {
	return &cache{
		Cache: c,
	}
}
func (c *cache) Set(f CacheObject) error {
	return c.Cache.Set(f.Key(), f.Content())
}
func (c *cache) SetWithExpire(f CacheObject) error {
	return c.Cache.SetWithExpire(f.Key(), f.Content(), f.Expire())
}
func (c *cache) Del(f CacheObject) error {
	return c.Cache.Del(f.Key())
}
func (c *cache) Get(f CacheObject, val interface{}) error {
	return c.Cache.Get(f.Key(), val)
}
