package xerr

/**(前3位代表业务,后三位代表具体功能)**/

/**全局错误码*/

const (
	// 成功返回
	OK uint32 = 200
	// 服务器开小差
	ServerCommonError uint32 = 100001
	// 请求参数错误
	ReuqestParamError uint32 = 100002
	// token过期
	TokenExpireError uint32 = 100003
	// 生成token失败
	TokenGenerateError uint32 = 100004
	// 数据库繁忙,请稍后再试
	DbError uint32 = 100005
	// 更新数据影响行数为0
	DbUpdateAffectedZeroError uint32 = 100006
	// 数据不存在
	DataNoExistError uint32 = 100007
	FailToModify     uint32 = 100008

	// 无权限
	NoPermission      uint32 = 400005
	NoUserInformation uint32 = 400001
	// 没有登入
	NotLoggedIn        uint32 = 400002
	SysErr             uint32 = 500000
	SysTimeout         uint32 = 500001
	IncorrectParameter uint32 = 500002
	AbnormalIdentity   uint32 = 500003

	// 课程
	CourseNotSign uint32 = 300001
)

var message = map[uint32]string{
	OK:                        "SUCCESS",
	ServerCommonError:         "服务器开小差啦,稍后再来试一试",
	ReuqestParamError:         "参数错误",
	TokenExpireError:          "token失效，请重新登陆",
	TokenGenerateError:        "生成token失败",
	DbError:                   "数据库繁忙,请稍后再试",
	DbUpdateAffectedZeroError: "更新数据影响行数为0",
	DataNoExistError:          "数据不存在",
	NoPermission:              "无权限",
	NoUserInformation:         "没有用户信息",
	NotLoggedIn:               "没有登入",
	FailToModify:              "修改失败",
	SysErr:                    "系统异常",
	SysTimeout:                "系统超时",
	IncorrectParameter:        "数据参数不正确",
	AbnormalIdentity:          "身份异常",
	CourseNotSign:             "课程没有报名",
}

var (
	ErrorDbErr              = NewErrCode(DbError)
	ErrorNoPermission       = NewErrCode(NoPermission)
	ErrorNoUserInformation  = NewErrCode(NoUserInformation)
	ErrorLoggedIn           = NewErrCode(NotLoggedIn)
	ErrorSysErr             = NewErrCode(SysErr)
	ErrorFailToModify       = NewErrCode(FailToModify)
	ErrorSysTimeout         = NewErrCode(SysTimeout)
	ErrorIncorrectParameter = NewErrCode(IncorrectParameter)
	ErrorAbnormalIdentity   = NewErrCode(AbnormalIdentity)
	ErrorCourseNotSign      = NewErrCode(CourseNotSign)
)

var (
	ErrorDataAdditionException = NewErrMsg("数据新增异常")
	ErrorNotLoggedIn           = NewErrMsg("请先登入")
	ErrorLogInElsewhere        = NewErrMsg("账号在其他地方登入")
)
