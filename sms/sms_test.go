package sms

import "testing"

func Test_sms_Send(t *testing.T) {
	
	type fields struct {
		servicer string
		cfg Config
	}
	type args struct {
		moblie  string
		content string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantCode int
		wantErr  bool
	}{
		{
			"1",
			fields{
				servicer: SmsBaoServiceProvider,
				cfg: map[string]interface{}{
					"User": "jinmin",
					"Pass": "www.jinmin.com",
				},
			},
			args{
				moblie:  "17388981374",
				content: "【云课堂】你的验证码为455637，如非本人请忽略",
			},
			Success,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			s := NewSms(tt.fields.servicer, tt.fields.cfg)

			gotCode, err := s.Send(tt.args.moblie, tt.args.content)
			if (err != nil) != tt.wantErr {
				t.Errorf("Send() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotCode != tt.wantCode {
				t.Errorf("Send() gotCode = %v, want %v", gotCode, tt.wantCode)
			}

			t.Logf("请求信息：%v", s.CodeText(gotCode))
		})
	}
}