package sms

import (
	"errors"
	"net/url"
	"strings"

	"gitee.com/dn-jinmin/pkg/tool"
	"gitee.com/dn-jinmin/pkg/tool/curl"
)

/*
	 短信宝服务

	 短信宝请求完后的状态码
	 $sodeStr = array(
		"0" => "短信发送成功",
		"-1" => "参数不全",
		"-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！",
		"30" => "密码错误",
		"40" => "账号不存在",
		"41" => "余额不足",
		"42" => "帐户已过期",
		"43" => "IP地址限制",
		"50" => "内容含有敏感词"
	 )
*/
type SmsPo struct {
	api   string
	user  string
	pass  string
	codes map[string]int
}

func NewSmsPo(cfg Config) *SmsPo {

	codes := map[string]int{
		"0":  Success,
		"-1": ConfigError,
		"30": IncorrectPassword,
		"40": AccountDoesNotExist,
		"50": ExistenceOfSensitiveWords,
		"43": RequestLimits,
	}

	sms := &SmsPo{
		api:   "http://api.smsbao.com/",
		codes: codes,
	}

	user, ok := cfg["User"].(string)
	if ok {
		sms.user = user
	}

	pass, ok := cfg["Pass"].(string)
	if ok {
		sms.pass = pass
	}

	return sms
}

func (s *SmsPo) Send(mobile, content string) (code int, err error) {
	// 整合短信宝api
	api := s.getApi(mobile, content)
	// 请求接口
	res, err := curl.Get(api)
	if err != nil {
		return SysFail, err
	}
	// 状态解析
	return s.codeParsing(string(res))
}

func (s *SmsPo) getApi(mobile, content string) string {

	var api strings.Builder
	api.WriteString(s.api)
	api.WriteString("sms?u=")
	api.WriteString(s.user)
	api.WriteString("&p=")
	api.WriteString(tool.Md5([]byte(s.pass)))
	api.WriteString("&m=")
	api.WriteString(mobile)
	api.WriteString("&c=")
	api.WriteString(url.QueryEscape(content))

	return api.String()
}

func (s *SmsPo) codeParsing(data string) (int, error) {
	code, ok := s.codes[data]
	if !ok {
		return SysFail, errors.New(codeText[SysFail])
	}
	return code, nil
}
