package sms

const (
	Success = iota
	ConfigError
	IncorrectPassword
	AccountDoesNotExist
	ExistenceOfSensitiveWords
	RequestLimits
	SysFail
)

var codeText = map[int]string{
	Success:                   "成功",
	ConfigError:               "配置出错",
	IncorrectPassword:         "密码不正确",
	AccountDoesNotExist:       "账号不存在",
	ExistenceOfSensitiveWords: "存在敏感词",
	RequestLimits:             "请求限制",
	SysFail:                   "系统出错",
}
