package sms

// 服务商类型
const (
	SmsBaoServiceProvider = "smsPo"
)

// 配置
type Config map[string]interface{}

// 服务商
type SmsService interface {
	Send(phone, content string) (code int, err error)
}

type SMS interface {
	SmsService
	CodeText(code int) string
}

type sms struct {
	smsSve SmsService
}

func NewSms(servicer string, cfg Config) SMS {
	var smsSve SmsService

	switch servicer {
	case SmsBaoServiceProvider:
		smsSve = NewSmsPo(cfg)
	default:
		smsSve = NewSmsPo(cfg)
	}

	return &sms{
		smsSve: smsSve,
	}
}

func (s *sms)Send(moblie, content string) (code int, err error) {
	return s.smsSve.Send(moblie,content)
}

func (s *sms) CodeText(code int) string {
	text,ok := codeText[code]
	if !ok {
		return codeText[SysFail]
	}
	return text
}
