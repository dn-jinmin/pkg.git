/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package mapstructurex

import (
	"encoding/json"
	"errors"
	"github.com/mitchellh/mapstructure"
	"reflect"
	"strconv"
	"time"
)

var InvalidFormDataError = errors.New("无效的form数据")

type InvalidDecodeError struct {
	Type reflect.Type
}

func (e *InvalidDecodeError) Error() string {
	if e.Type == nil {
		return "mapstructurex: Decode(nil)"
	}

	if e.Type.Kind() != reflect.Pointer {
		return "mapstructurex: Decode(non-pointer " + e.Type.String() + ")"
	}
	return "mapstructurex: Decode(nil " + e.Type.String() + ")"
}

const (
	Tag       = "mapstructure"
	TimeLayui = "2006-01-02 15:04:05 "
)

const (
	Time        = "time.Time"
	SqlNullTime = "sql.NullTime"
)

func Decode(form interface{}, param interface{}) error {

	paramValue := reflect.ValueOf(param)
	paramType := reflect.TypeOf(param)
	// 校验数据
	if err := checkVail(form, paramValue, paramType); err != nil {
		return err
	}

	paramValue = paramValue.Elem()
	paramType = paramType.Elem()

	switch f := form.(type) {
	case map[string]string:
		return valueStrDecode(f, paramValue, paramType)
	case []map[string]string:
		data := reflect.MakeSlice(paramType, len(f), len(f))
		for i, v := range f {
			e := data.Index(i)
			var node reflect.Value
			switch e.Type().Kind() {
			case reflect.Struct:
				if err := valueStrDecode(v, e, e.Type()); err != nil {
					return err
				}
			case reflect.Ptr:
				node = reflect.New(e.Type().Elem())
				if err := valueStrDecode(v, node.Elem(), node.Type().Elem()); err != nil {
					return err
				}
				e.Set(node)
			}
		}
		paramValue.Set(data)
		return nil
	default:
		return mapstructure.Decode(form, param)
	}
}

func checkVail(form interface{}, paramValue reflect.Value, paramType reflect.Type) error {
	if form == nil {
		return InvalidFormDataError
	}
	if paramType.Kind() != reflect.Ptr || paramType.Kind() != reflect.Pointer || paramValue.IsNil() {
		return &InvalidDecodeError{
			Type: paramType,
		}
	}
	return nil

}

// map[string]string 解析为 struct
func valueStrDecode(form map[string]string, paramValue reflect.Value, paramType reflect.Type) error {

	if paramType.Kind() == reflect.Ptr {
		paramValue = paramValue.Elem()
		paramType = paramType.Elem()
	}

	for i := 0; i < paramType.NumField(); i++ {
		// 获取到字段
		field := paramType.Field(i)
		// 获取tag
		tag := field.Tag.Get(Tag)
		// 从map中获取值
		value, ok := form[tag]
		if !ok {
			continue
		}

		if err := setField(value, paramValue.Field(i), field.Type); err != nil {
			return err
		}
	}
	return nil
}

// 设置字段值
func setField(value string, fieldValue reflect.Value, fieldType reflect.Type) error {
	switch fieldValue.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if value == "" {
			fieldValue.SetInt(0)
			return nil
		}

		v, err := strconv.Atoi(value)
		if err != nil {
			return err
		}
		fieldValue.SetInt(int64(v))
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		if value == "" {
			fieldValue.SetInt(0)
			return nil
		}
		v, err := strconv.Atoi(value)
		if err != nil {
			return err
		}
		fieldValue.SetUint(uint64(v))
	case reflect.String:
		fieldValue.SetString(value)
	case reflect.Map:
		// 待完善
		//var arrs map[string]string
		//if err := json.Unmarshal([]byte(value), &arrs); err != nil {
		//	return err
		//}
		//new := reflect.MakeMap(fieldType)
		//
		//for k, v := range arrs {
		//
		//}
		//err := valueStrDecode(arrs, new, new.Type())
		//if err != nil {
		//	return err
		//}
	case reflect.Struct:
		switch fieldValue.Type().String() {
		case Time:
			return setParseTime(fieldValue, value)
		case SqlNullTime:
			return setParseTime(fieldValue.FieldByName("Time"), value)
		default:
			new := reflect.New(fieldType)
			if value == "" {
				fieldValue.Set(new.Elem())
				return nil
			}
			var v map[string]string
			if err := json.Unmarshal([]byte(value), &v); err != nil {
				return err
			}
			if err := valueStrDecode(v, new, new.Type()); err != nil {
				return err
			}
			fieldValue.Set(new.Elem())
		}
	case reflect.Ptr:
		new := reflect.New(fieldType.Elem())
		if value == "" {
			fieldValue.Set(new)
			return nil
		}
		var v map[string]string
		if err := json.Unmarshal([]byte(value), &v); err != nil {
			return err
		}
		if err := valueStrDecode(v, new.Elem(), new.Type().Elem()); err != nil {
			return err
		}
		fieldValue.Set(new)
	case reflect.Slice:
		if value == "" {
			fieldValue.Set(reflect.MakeSlice(fieldType, 0, 0))
			return nil
		}
		var arrs []string
		if err := json.Unmarshal([]byte(value), &arrs); err != nil {
			return err
		}
		if len(arrs) == 0 {
			return nil
		}
		new := reflect.MakeSlice(fieldType, len(arrs), len(arrs))
		for i := 0; i < new.Cap(); i++ {
			e := new.Index(i)
			setField(arrs[i], e, e.Type())
		}
		fieldValue.Set(new)
	}
	return nil
}

// 设置时间类型
func setParseTime(field reflect.Value, value string) error {
	if value == "0000-00-00 00:00:00" || value == "" {
		return nil
	}

	local, _ := time.LoadLocation("Asia/Shanghai")
	showTime, err := time.ParseInLocation(TimeLayui, value, local)
	if err != nil {
		return err
	}
	field.Set(reflect.ValueOf(showTime))
	return nil
}
