/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package mapstructurex

import (
	"database/sql"
	"fmt"
	"reflect"
	"testing"
	"time"
)

func Test_Decode1(t *testing.T) {
	type RoleInfo struct {
		Title string `mapstructure:"title"`
	}

	type Role struct {
		Id       int64     `mapstructure:"id"`
		Name     string    `mapstructure:"name"`
		RoleInfo *RoleInfo `mapstructure:"role_info"`
	}

	type Config struct {
		Addr string `mapstructure:"addr"`
	}

	type Photo struct {
		Path string `mapstructure:"path"`
	}

	type User struct {
		Id       int64        `mapstructure:"id"`
		Name     string       `mapstructure:"name"`
		CreateAt time.Time    `mapstructure:"create_at"`
		UpdateAt sql.NullTime `mapstructure:"update_at"`
		Role     *Role        `mapstructure:"role"`
		Title    string
		Config   Config   `mapstructure:"config"`
		Photos   []*Photo `mapstructure:"photos"`
		Hobby    []string `mapstructure:"hobby"`
	}

	type args struct {
		form map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"1", args{form: map[string]string{
			"id":        "1",
			"name":      "user",
			"create_at": "0000-00-00 00:00:00",
			"update_at": "0000-00-00 00:00:00",
			"title":     "mapstructure",
			"role":      "{\"id\":\"100\",\"name\":\"老师\",\"role_info\":\"{\\\"title\\\":\\\"info详情\\\"}\"}",
			"config":    "{\"addr\":\"127.0.0.1:8000\"}",
			"photos":    "[\"{\\\"path\\\":\\\"/xx/1.jpg\\\"}\",\"{\\\"path\\\":\\\"/xx/2.jpg\\\"}\"]",
			"hobby":     "[\"篮球\",\"游戏\",\"编程\"]",
		}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var user User
			t.Log(Decode(tt.args.form, &user))
			t.Log(user.CreateAt)
			t.Log(user.Config)
			t.Log(user.Role)
			t.Log(user.Role.RoleInfo)
			t.Log(user.Hobby)
			for _, photo := range user.Photos {
				t.Log("==>>", photo)
			}
		})
	}
}

func Test_Decode2(t *testing.T) {
	type User struct {
		Id       int64        `mapstructure:"id"`
		Name     string       `mapstructure:"name"`
		CreateAt time.Time    `mapstructure:"create_at"`
		UpdateAt sql.NullTime `mapstructure:"update_at"`
		Title    string
		Hobby    []string `mapstructure:"hobby"`
	}
	type args struct {
		form []map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"1", args{form: []map[string]string{
			{
				"id":        "1",
				"name":      "user",
				"create_at": "2006-01-02 15:04:05",
				"update_at": "0001-01-01 00:00:00",
				"title":     "mapstructure",
				"config":    "{\"addr\":\"127.0.0.1:8000\"}",
				"hobby":     "[\"篮球\",\"游戏\",\"编程\"]",
			},
			{
				"id":        "2",
				"name":      "user",
				"create_at": "2007-01-02 15:04:05",
				"update_at": "0000-00-00 00:00:00",
				"config":    "{\"addr\":\"127.0.0.1:8000\"}",
				"hobby":     "[\"go\",\"java\",\"php\"]",
			},
		}}, true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var users []User
			t.Log(Decode(tt.args.form, &users))
			for _, user := range users {
				t.Log(user)
			}

			var users2 []*User
			t.Log(Decode(tt.args.form, users2))
			for _, user := range users2 {
				t.Log(user)
			}
		})
	}
}

func TestRef_SetTime(t *testing.T) {
	var CreateAt time.Time

	valueOfA := reflect.ValueOf(&CreateAt).Elem()
	//valueOfA.Set(reflect.ValueOf("2006-01-02T15:04:05Z07:00"))
	local, _ := time.LoadLocation("Asia/Shanghai")
	showTime, err := time.ParseInLocation(TimeLayui, TimeLayui, local)
	fmt.Println(showTime, err)

	valueOfA.Set(reflect.ValueOf(showTime))

	fmt.Println(CreateAt)
}

func TestSqlTime(t *testing.T) {
	var UpdateAt sql.NullTime
	valueOfA := reflect.ValueOf(&UpdateAt).Elem()
	fmt.Println(valueOfA.Type().String())
	local, _ := time.LoadLocation("Asia/Shanghai")
	showTime, err := time.ParseInLocation(TimeLayui, TimeLayui, local)
	fmt.Println(showTime, err)

	valueOfA.FieldByName("Time").Set(reflect.ValueOf(showTime))
	fmt.Println(UpdateAt)
}

func TestRef_SetInt(t *testing.T) {
	var a int64 = 1

	valueOfA := reflect.ValueOf(&a).Elem()
	valueOfA.SetInt(2)

	fmt.Println(a)
}
