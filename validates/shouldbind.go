package validates

import (
	"github.com/zeromicro/go-zero/rest/httpx"

	"github.com/go-playground/validator/v10"
	"net/http"
)

type Validate struct {
	v *validator.Validate
}

func NewValidate(local string) *Validate {

	v := validator.New()

	InitValidate(v, nil, local)

	return &Validate{
		v: validator.New(),
	}
}

func (val *Validate) ShouldBind(r *http.Request, v interface{}) error {

	if err := httpx.Parse(r, v); err != nil {
		return err
	}

	if err := val.v.Struct(v); err != nil {
		return err
	}

	return nil
}

