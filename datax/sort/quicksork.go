/**
 * @author: dn-jinmin/dn-jinmin
 * @doc: 快速排序
 */
package sort

var Count = 0

var Deta = false

func QuickSorkInt32(arr []int32) []int32 {
	quickSorkInt32(arr, 0, len(arr)-1)
	return arr
}

func quickSorkInt32(arr []int32, start, end int) {
	if start < end {
		mid := partitionInt32(arr, start, end)
		// 左边
		quickSorkInt32(arr, start, mid-1)
		// 右边
		quickSorkInt32(arr, mid+1, end)
	}
}

func partitionInt32(arr []int32, start, end int) int {

	for start < end {
		// 从右往左与基数比较，如果基数大于右侧值则交换
		//fmt.Printf("==>> arr[start] < arr[end] : start = %d ; end = %d ; arr = %v \n", start, end, arr)
		for start < end && arr[start] <= arr[end] {
			if Deta {
				Count++
			}

			end--
		}
		// 目前基数为 mid := arr[start] 交换为了 mid := arr[end]
		arr[start], arr[end] = arr[end], arr[start]
		// 从左往右与基数比较，如果基数小于左侧值则交换
		//fmt.Printf("==>> arr[start] > arr[end] : start = %d ; end = %d ; arr = %v\n", start, end, arr)
		for start < end && arr[end] >= arr[start] {
			if Deta {
				Count++
			}
			start++
		}
		// 目前基数为 mid := arr[end] 交换为了 mid := arr[start]
		arr[start], arr[end] = arr[end], arr[start]
	}

	return start
}

//通过
//func partitionInt32(arr []int32, start, end int) int {
//	pivot := arr[start]
//	for start < end {
//		for start < end && pivot <= arr[end] {
//			end--
//		}
//		// 填补 low 位置空值
//		arr[start] = arr[end]
//		for start < end && pivot >= arr[start] {
//			start++
//		}
//		// 填补end位置空值
//		arr[end] = arr[start]
//	}
//	//pivot 填补 low位置的空值
//	arr[start] = pivot
//	return start
//}

//不通过
//func partitionInt32(nums []int32, l, r int) int {
//	key := nums[r]
//
//	i := l
//	j := l
//
//	for j < r {
//		if nums[i] < key {
//			nums[i], nums[j] = nums[j], nums[i]
//			i++
//		}
//		j++
//	}
//	nums[i], nums[r] = nums[r], nums[i]
//	return i
//}
