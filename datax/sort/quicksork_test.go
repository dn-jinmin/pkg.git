/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package sort

import (
	"math/rand"
	"testing"
	"time"
)

func TestQuickSorkInt32(t *testing.T) {
	rand.Seed(time.Now().Unix())
	var arr []int32
	for i := 0; i < 100; i++ {
		arr = append(arr, int32(rand.Intn(1000)))
	}
	t.Log("==>>", arr)

	type args struct {
		arr []int32
	}
	tests := []struct {
		name string
		args args
		want []int32
	}{
		{"1", args{arr: arr}, []int32{1, 3, 6}},
		{"2", args{arr: []int32{3, 7, 1, 10, 7}}, []int32{1, 3, 7, 7, 10}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Log("排序", tt.args.arr)
			got := QuickSorkInt32(tt.args.arr)
			t.Log("got : ", got)
			t.Log("count : ", Count)
		})
	}
}
