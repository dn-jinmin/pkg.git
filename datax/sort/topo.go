/**
 * @Author: biwentao
 * @Description:
 * @File:  topu
 * @Version: 1.0.0
 * @Date: 2020/11/24 下午1:51
 */
package sort

import "sort"

// 拓扑排序
func TopoSort(m map[string][]string) []string {
	var order []string
	seen := make(map[string]bool)
	var visitAll func(items []string)
	visitAll = func(items []string) {
		for _, item := range items {
			if !seen[item] {
				seen[item] = true
				visitAll(m[item])
				order = append(order, item)
			}
		}
	}
	var keys []string
	for key := range m {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	visitAll(keys)
	return order
}
