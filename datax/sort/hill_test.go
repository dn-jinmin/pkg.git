/**
 * @Author: liaoPengLiang
 * @File:  sort_test
 * @Version: 1.0.0
 * @Date: 2023/3/23
 * @Description:
 */

package sort

import (
	"reflect"
	"testing"
)

func TestHillSort(t *testing.T) {

	type args struct {
		arr []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"ceshi", args{arr: []int{7, 2, 4, 1, 3, 6, 2}}, []int{1, 2, 2, 3, 4, 6, 7}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HillSort(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("HillSort() = %v, want %v", got, tt.want)
			}
		})
	}
}
