package sort

import (
	"runtime"
	"sync"
)

func hillSortStep(arr []int, start int, gap int) {
	length := len(arr)
	for i := start + gap; i < length; i += gap {
		backup := arr[i]
		j := i - gap
		for j >= 0 && backup < arr[j] {
			arr[j+gap] = arr[j]
			j -= gap
		}
		arr[j+gap] = backup
	}
}

// 希尔排序(并发排序)
func HillSort(arr []int) []int {
	if len(arr) < 2 || arr == nil {
		return arr
	}
	cpunum := runtime.NumCPU()
	wg := sync.WaitGroup{}

	// 压缩空间
	for gap := len(arr); gap > 0; gap /= 2 {
		wg.Add(cpunum)
		ch := make(chan int, 10000)
		go func() {
			for k := 0; k < gap; k++ {
				ch <- k
			}
			close(ch)
		}()
		for k := 0; k < cpunum; k++ {
			go func() {
				for v := range ch {
					hillSortStep(arr, v, gap)
				}
				wg.Done()
			}()
		}
		wg.Wait()
	}
	return arr
}
