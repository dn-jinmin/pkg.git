/**
 * @Author: biwentao
 * @Description:
 * @File:  topo_test
 * @Version: 1.0.0
 * @Date: 2020/11/24 下午1:51
 */
package sort

import "testing"

func TestTopoSort(t *testing.T) {
	var prereqs = map[string][]string{
		"algorithms": {"data structures"},
		"calculus":   {"linear algebra"},
		"compilers": {
			"data structures",
			"formal languages",
			"computer organization",
		},
		"data structures":       {"discrete math"},
		"databases":             {"data structures"},
		"discrete math":         {"intro to programming"},
		"formal languages":      {"discrete math"},
		"networks":              {"operating systems"},
		"operating systems":     {"data structures", "computer organization"},
		"programming languages": {"data structures", "computer organization"},
	}
	for i, course := range TopoSort(prereqs) {
		t.Logf("%d:\t%s\n", i+1, course)
	}
}
