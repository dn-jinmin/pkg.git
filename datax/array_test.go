/**
 * @Author: liaoPengLiang
 * @File:  pkg_test
 * @Version: 1.0.0
 * @Date: 2023/3/16
 * @Description:
 */

package pkg

import (
	"testing"
)

func TestDeweight(t *testing.T) {
	type args struct {
		firstArr  []string
		secondArr []string
	}
	tests := []struct {
		name string
		args args
	}{
		{"1", args{
			firstArr:  []string{"a", "b", "c", "d"},
			secondArr: []string{"a", "p", "l"},
		}},
		{"2", args{
			firstArr:  []string{"a", "p", "l"},
			secondArr: []string{"a", "b", "c", "d"},
		}},
		{"3", args{
			firstArr:  []string{"a", "p", "l"},
			secondArr: []string{"f", "b", "c"},
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Deweight(tt.args.firstArr, tt.args.secondArr)
			t.Log("name : ", tt.name, " got : ", got)
		})
	}
}

//func TestNode(t *testing.T) {
//	arrs := [][]int{
//		{1, 2, 8},
//		{22, 6},
//		{1},
//		{1, 2, 8, 3},
//		{1, 4},
//		{22},
//	}
//
//	// {3, 4, 6}
//	// 1-2-3, 1-2-8, 1-4, 22-6
//	var (
//		res     = make([]int, 0)
//		records = make(map[int]int)
//		arrPath = make(map[int]int)
//	)
//
//	t.Log(res, arrPath)
//}

func Test_getLeafNode(t *testing.T) {
	type args struct {
		arrs [][]int
	}
	tests := []struct {
		name string
		args args
	}{
		{"1", args{arrs: [][]int{
			{1, 2, 8},
			{22, 6},
			{1},
			{1, 2, 8, 3},
			{1, 4},
			{22},
		}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLeafNode, gotLeafarr := getLeafNode(tt.args.arrs)
			//if !reflect.DeepEqual(gotLeafNode, tt.wantLeafNode) {
			//	t.Errorf("getLeafNode() gotLeafNode = %v, want %v", gotLeafNode, tt.wantLeafNode)
			//}
			DD("gotLeafNode", gotLeafNode)
			DD("gotLeafarr", gotLeafarr)
		})
	}
}
