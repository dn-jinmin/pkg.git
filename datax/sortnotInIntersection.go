/**
 * @author: dn-jinmin/dn-jinmin
 * @doc: 交集处理
 */
package datax

import "gitee.com/dn-jinmin/pkg/datax/seek"

// Sort Not In Intersection
//
//	@Description: 计算两个数组不在交集内的元素
//	@param gist
//	@param target
//	@return []int32
func TargetNoInSortGistInt32(gist []int32, target []int32, order int) []int32 {
	data := make([]int32, 0, len(target))

	for _, v := range target {
		if n := seek.BinarySearchInt32(gist, v, order); n == -1 {
			data = append(data, v)
		}
	}
	return data
}
