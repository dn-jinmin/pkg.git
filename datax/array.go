/**
 * @Author: liaoPengLiang
 * @File:  pkg
 * @Version: 1.0.0
 * @Date: 2023/3/16
 * @Description:
 */

package pkg

//
// Deweight
//  @Description: 两个数组去重
//  @param firstArr
//  @param secondArr
//  @return []string
//
func Deweight(firstArr, secondArr []string) []string {
	record := make(map[string]string, len(firstArr))

	firstLen := len(firstArr)
	secondLen := len(secondArr)

	if firstLen > secondLen {
		return deweight(firstLen, secondLen, firstArr, secondArr, record)
	}
	return deweight(secondLen, firstLen, secondArr, firstArr, record)
}

func deweight(maxArrLen, minArrLen int, firstMaxArr, secondMinArr []string, record map[string]string) []string {
	res := make([]string, 0, maxArrLen)
	for i := 0; i < maxArrLen; i++ {
		// 判断第一个数组是否存在
		_, ok := record[firstMaxArr[i]]
		if !ok {
			record[firstMaxArr[i]] = firstMaxArr[i]
			res = append(res, firstMaxArr[i])
		}

		if i > minArrLen-1 {
			continue
		}
		// 判断第二个数组是否存在
		_, ok = record[secondMinArr[i]]
		if !ok {
			res = append(res, secondMinArr[i])
			record[secondMinArr[i]] = secondMinArr[i]
		}
	}

	return res
}

func getLeafNode(arrs [][]int) (leafNode []int, leafarr [][]int) {
	leafNode = make([]int, 0, 1)
	records := make(map[int]int, 1)
	arrPath := make(map[int]int, len(arrs))

	for arridx, arr := range arrs {
		for i := len(arr) - 1; i >= 0; i-- {
			record, ok := records[arr[i]]
			if ok {
				if record == -1 {
					break
				}
				leafNode[record] = -1
			} else {
				if i == len(arr)-1 {
					leafNode = append(leafNode, arr[i])
					records[arr[i]] = len(leafNode) - 1
					arrPath[arr[i]] = arridx
				} else {
					records[arr[i]] = -1
				}
			}
		}
	}

	leafarr = make([][]int, 0, len(leafNode))
	for _, i := range leafNode {
		if i == -1 {
			continue
		}
		leafarr = append(leafarr, arrs[arrPath[i]])
	}

	return leafNode, leafarr
}
