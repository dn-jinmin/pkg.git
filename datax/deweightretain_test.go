/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */

package datax

import (
	"testing"
)

func TestSortTargetCheckDeweightInt32(t *testing.T) {
	type args struct {
		gist   []int32
		target []int32
	}
	tests := []struct {
		name string
		args args
		want []int32
	}{
		{"1", args{
			gist:   []int32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
			target: []int32{2, 3, 7, 20},
		}, []int32{20}},
		{"2", args{
			gist:   []int32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
			target: []int32{2, 3, 7},
		}, []int32{}},
		{"2", args{
			gist:   []int32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
			target: []int32{30, 39, 40},
		}, []int32{30, 39, 40}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Deta = true
			//got := SortTargetCheckDeweightInt32(tt.args.gist, tt.args.target)
			//if !reflect.DeepEqual(got, tt.want) {
			//	t.Errorf("SortTargetCheckDeweightInt32() = %v, want %v", got, tt.want)
			//}
			//t.Log("got := ", got)
			//t.Log("count := ", Count)
			//Count = 0
		})
	}
}
