/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:排序去重,目标数据为数值类型
 */

package datax

import (
	"gitee.com/dn-jinmin/pkg/datax/seek"
)

var Count = 0
var Deta = false

// 基于目标数组校验去重; 前提两个数组均有排序
func SortTargetCheckDeweightInt32(gist []int32, target []int32, order int) []int32 {

	data := make([]int32, 0, len(target))

	for _, v := range target {
		if n := seek.BinarySearchInt32(gist, v, order); n == -1 {
			data = append(data, v)
		}
	}

	return data
}

//func sortTargetCheckDeweightInt32(data *[]int32, gist []int32, target []int32, gistIdx, targetIdx int) {
//
//	if targetIdx > len(target)-1 || gistIdx > len(gist)-1 {
//		return
//	}
//
//	for ; targetIdx < len(target)-1; targetIdx++ {
//		if Deta {
//			Count++
//		}
//		if gist[gistIdx] != target[targetIdx] {
//			break
//		}
//	}
//
//	if targetIdx > len(target)-1 {
//		return
//	}
//
//	gistIdx++
//
//	n, left, _ := seek.BinarySearchInt32AndCoord(gist[gistIdx:], target[targetIdx])
//
//	if n == -1 {
//		// 记录不重复的
//		*data = append(*data, target[targetIdx])
//	}
//	targetIdx++
//	sortTargetCheckDeweightInt32(data, gist, target, left+1, targetIdx)
//	return
//}
