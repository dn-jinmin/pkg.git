/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package tool

// order
const (
	// 小到大
	LittleEndian = iota
	// 大到小
	BigEndian
)

// comparator
const (
	// 等于
	EQ = iota
	// 小于
	LT
	// 大于
	GT
	// 小于等于
	LE
	// 不等于
	NE
	// 大于等于
	GE
)
