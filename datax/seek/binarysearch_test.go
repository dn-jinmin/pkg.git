/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package seek

import (
	"gitee.com/dn-jinmin/pkg/tool"
	"gitee.com/dn-jinmin/gen-id/utils"
	"testing"
	"time"

	"gitee.com/dn-jinmin/pkg/datax"
)

func TestBinarySearchInt32(t *testing.T) {
	type args struct {
		arr    []int32
		target int32
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"1", args{arr: []int32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, target: 5}, 4},
		{"2", args{arr: []int32{1, 2, 3, 4, 5, 6, 7, 9, 10}, target: 7}, 6},
		{"3", args{arr: []int32{1, 2}, target: 7}, -1},
		{"4", args{arr: []int32{1, 2, 3}, target: 3}, 2},
		{"5", args{arr: []int32{1, 2, 3, 9}, target: 3}, 2},
		{"131", args{arr: []int32{131, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}, target: 131}, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := BinarySearchInt32(tt.args.arr, tt.args.target, datax.LittleEndian)
			if got != tt.want {
				t.Errorf("BinarySearchInt32() = %v, want %v", got, tt.want)
			}

			if got != -1 {
				t.Logf("arr := %d ", tt.args.arr[got])
			}
		})
	}
}

func TestBinarySearchInt32AndCoord(t *testing.T) {
	type args struct {
		arr    []int32
		target int32
	}
	tests := []struct {
		name      string
		args      args
		wantN     int
		wantLeft  int
		wantRight int
	}{
		//{"1", args{
		//	arr:    []int32{1, 2, 3, 4, 5, 6, 80},
		//	target: 20,
		//}, -1, 5, 5},
		{"2", args{
			arr:    []int32{1, 2, 3, 7, 9, 30, 80},
			target: 20,
		}, -1, 5, 5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotN, gotLeft, gotRight := BinarySearchInt32AndCoord(tt.args.arr, tt.args.target, datax.LittleEndian)
			if gotN != tt.wantN {
				t.Errorf("BinarySearchInt32AndCoord() gotN = %v, want %v", gotN, tt.wantN)
			}
			if gotLeft != tt.wantLeft {
				t.Errorf("BinarySearchInt32AndCoord() gotLeft = %v, want %v", gotLeft, tt.wantLeft)
			}
			if gotRight != tt.wantRight {
				t.Errorf("BinarySearchInt32AndCoord() gotRight = %v, want %v", gotRight, tt.wantRight)
			}

			t.Log(gotN, gotLeft, gotRight)
		})
	}
}

func TestBinarySearchAny(t *testing.T) {
	arr := []int{1, 4, 6, 8, 10}

	type User struct {
		Id   int
		Age  int
		Name string
	}
	// 根据age排序
	userlist := []User{
		{1, 10, "alin"}, {2, 10, "alin2"}, {3, 14, "lili"}, {3, 18, "xiaoxi"}, {4, 20, "yu"}, {4, 26, "xiaoming"},
	}
	type args struct {
		len     int
		order   int
		compare Compare
		print   func(int)
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"1", args{
				len:   len(arr),
				order: datax.LittleEndian,
				compare: func(middle int) int {
					target := 8
					if arr[middle] == target {
						return datax.EQ
					}
					if arr[middle] > target {
						return datax.GT
					} else {
						return datax.LT
					}
				},
				print: func(i int) {
					t.Log("data := ", arr[i])
				},
			}, 3,
		}, {
			"2", args{
				len:   len(userlist),
				order: datax.LittleEndian,
				compare: func(middle int) int {
					target := 18
					if userlist[middle].Age == target {
						return datax.EQ
					}
					if userlist[middle].Age > target {
						return datax.GT
					} else {
						return datax.LT
					}
				}, print: func(i int) {
					t.Log("data := ", userlist[i])
				},
			}, 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got := BinarySearchAny(tt.args.len, tt.args.order, tt.args.compare)

			if got != tt.want {
				t.Errorf("BinarySearchAny() = %v, want %v", got, tt.want)
			}

			tt.args.print(got)
		})
	}
}

func TestBinarySearchRangeAnyObject(t *testing.T) {
	// 准备测试数据
	type user struct {
		Id   int
		Bday time.Time
	}
	var (
		users    []*user
		sustains = []int{30, 30, 30, 30, 30, 30, 30, 60, 120, 180, 240, 300, 360, 1440}
		timer    = randTime()
	)
	t.Log("====== >>> 准备的数据 start")
	for i := 0; i < 10; i++ {
		u := &user{
			Id:   i,
			Bday: timer,
		}
		users = append(users, u)
		timer = timer.Add(time.Duration(sustains[utils.RUint(len(sustains))]) * time.Minute)
		t.Log(u)
	}
	t.Log("====== >>> 准备的数据 end")

	// 时间范围
	timerStart := timer.Add(-(12 * time.Hour))
	timerEnd := timer.Add(-(4 * time.Hour))
	t.Log(" timerStart ", timerStart, " timerEnd ", timerEnd)
	// 基于范围处理
	n := BinarySearchRangeAny(len(users), datax.LittleEndian, func(middle int) int {
		t.Log("users[middle].Bday : ", users[middle].Bday, " middle ", middle)

		if users[middle].Bday.Equal(timerStart) || users[middle].Bday.Equal(timerEnd) {
			t.Log("等于 users[middle].Bday.Equal(timerStart) || users[middle].Bday.Equal(timerEnd)")
			return datax.EQ
		}

		if !users[middle].Bday.After(timerStart) {
			t.Log("小于 users[middle].Bday.After(timerStart)")
			// 匹配时间小于开始时间
			return datax.LT
		}

		if users[middle].Bday.Before(timerEnd) {
			t.Log("在范围内 users[middle].Bday.Before(timerEnd)")
			return datax.EQ
		}
		t.Log("大于")
		return datax.GT
	}, func(idx int) bool {
		t.Log("===== >>>> start", idx)
		t.Log(users[idx])
		t.Log("===== >>>> end", idx)
		return true
	})

	t.Log("n ", n)
}

func TestLittleBinarySearchAny(t *testing.T) {
	arr := []int{1, 4, 6, 8, 10}

	type args struct {
		idx     int
		last    int
		compare Compare
		print   func(n, left, right int)
	}
	tests := []struct {
		name      string
		args      args
		wantN     int
		wantLeft  int
		wantRight int
	}{
		{"1", args{
			idx:  0,
			last: len(arr) - 1,
			compare: func(middle int) int {
				t.Log("middle ", middle)
				target := 70
				if arr[middle] == target {
					return datax.EQ
				}
				if arr[middle] > target {
					return datax.GT
				} else {
					return datax.LT
				}
			},
			print: func(n, left, right int) {
				if n != -1 {
					t.Log(" data = ", arr[n])
				}
				t.Log(" left = ", left, " right = ", right)
			},
		}, 3, 3, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotN, gotLeft, gotRight := LittleBinarySearchAny(tt.args.idx, tt.args.last, tt.args.compare)
			//if gotN != tt.wantN {
			//	t.Errorf("LittleBinarySearchAny() gotN = %v, want %v", gotN, tt.wantN)
			//}
			//if gotLeft != tt.wantLeft {
			//	t.Errorf("LittleBinarySearchAny() gotLeft = %v, want %v", gotLeft, tt.wantLeft)
			//}
			//if gotRight != tt.wantRight {
			//	t.Errorf("LittleBinarySearchAny() gotRight = %v, want %v", gotRight, tt.wantRight)
			//}

			if tt.args.print != nil {
				tt.args.print(gotN, gotLeft, gotRight)
			}
		})
	}
}

type task struct {
	Start time.Time
	End   time.Time
}

func TestLittleBinarySearchAny2(t *testing.T) {
	tasks := []*task{
		{
			Start: tool.StringToTime("2022-12-21 8:30:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-21 9:00:00", tool.DefaultTimeLayout),
		}, {
			Start: tool.StringToTime("2022-12-22 8:30:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-22 9:00:00", tool.DefaultTimeLayout),
		}, {
			Start: tool.StringToTime("2022-12-22 9:30:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-22 10:00:00", tool.DefaultTimeLayout),
		}, {
			Start: tool.StringToTime("2022-12-22 10:30:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-22 11:00:00", tool.DefaultTimeLayout),
		}, {
			Start: tool.StringToTime("2022-12-22 11:00:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-22 11:30:00", tool.DefaultTimeLayout),
		}, {
			Start: tool.StringToTime("2022-12-22 12:30:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-22 13:00:00", tool.DefaultTimeLayout),
		}, {
			Start: tool.StringToTime("2022-12-23 13:00:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-23 13:30:00", tool.DefaultTimeLayout),
		}, {
			Start: tool.StringToTime("2022-12-23 13:30:00", tool.DefaultTimeLayout),
			End:   tool.StringToTime("2022-12-23 14:00:00", tool.DefaultTimeLayout),
		},
	}

	now := tool.StringToTime("2022-12-23 12:55:00", tool.DefaultTimeLayout)
	currentTime := now.Add(5 * time.Minute)
	nextTime := currentTime.Add(30 * time.Minute)
	t.Log("current time ", currentTime)
	t.Log("next time ", nextTime)
	LinaryBinarySearchRangeAny(len(tasks), func(middle int) int {
		return compareTaskTime(tasks[middle], currentTime, nextTime)
	}, func(idx int) (notBreakUp bool) {
		t.Log(tasks[idx])
		return true
	})
}

func compareTaskTime(task *task, currentTime, nextTime time.Time) (compare int) {
	if task.Start.Equal(currentTime) || task.Start.Equal(nextTime) {
		return datax.EQ
	}
	if task.End.Before(currentTime) || task.End.Before(nextTime) {
		return datax.LT
	}

	if task.Start.After(currentTime) || task.Start.After(nextTime) {
		return datax.GT
	}
	// 才开始课程
	return datax.NE

}

func randTime() time.Time {
	now := time.Now()
	if idx := utils.RUint(5); idx > 1 {
		return now.Add(time.Duration(idx*24) * time.Hour)
	}
	return now
}
