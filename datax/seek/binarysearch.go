/**
 * @author: dn-jinmin/dn-jinmin
 * @doc: 二分查找
 */
package seek

import (
	"gitee.com/dn-jinmin/pkg/datax"
)

// BinarySearchInt32
//
//	@Description: 二分查找元素是否存在
//	@param arr
//	@param target
//	@return int
func BinarySearchInt32(arr []int32, target int32, order int) int {
	n, _, _ := BinarySearchInt32AndCoord(arr, target, order)
	return n
}

// BinarySearchInt32AndCoord
//
//	@Description: 二分查找元素是否存在，并返回查找的最终位置
//	@param arr
//	@param target
//	@return n
//	@return left
//	@return right
func BinarySearchInt32AndCoord(arr []int32, target int32, order int) (n int, left int, right int) {
	switch order {
	case datax.BigEndian:
		return bigBinarySearchInt32(arr, target, 0, len(arr)-1)
	case datax.LittleEndian:
		return littleBinarySearchInt32(arr, target, 0, len(arr)-1)
	}
	return -1, 0, 0
}

// littleBinarySearchInt32
//
//	@Description: 二分查找元素
//	@param arr 数据集合 | 小到大的顺序
//	@param target 查找数据
//	@param idx 开始坐标
//	@param last 结束坐标
//	@return n 元素位置
//	@return left 查找结束位置 - 左边
//	@return right 查找结束位置 - 右边
func littleBinarySearchInt32(arr []int32, target int32, idx int, last int) (n int, left int, right int) {
	// 终止条件
	if idx > last {
		return -1, last, idx
	}

	middle := (idx + last) / 2

	// 找到元素的情况
	if arr[middle] == target {
		return middle, idx, last
	}
	// 未找到
	if arr[middle] < target {
		return littleBinarySearchInt32(arr, target, middle+1, last)
	} else {
		return littleBinarySearchInt32(arr, target, idx, middle-1)
	}
}

// bigBinarySearchInt32
//
//	@Description: 二分查找元素
//	@param arr 数据集合 | 大到小的顺序
//	@param target 查找数据
//	@param idx 开始坐标
//	@param last 结束坐标
//	@return n 元素位置
//	@return left 查找结束位置 - 左边
//	@return right 查找结束位置 - 右边
func bigBinarySearchInt32(arr []int32, target int32, idx int, last int) (n int, left int, right int) {
	// 终止条件
	if idx < last {
		return -1, idx, last
	}

	middle := (idx + last) / 2
	// 找到元素的情况
	if arr[middle] == target {
		return middle, idx, last
	}
	// 未找到
	if arr[middle] < target {
		return bigBinarySearchInt32(arr, target, idx, middle-1)
	} else {
		return bigBinarySearchInt32(arr, target, middle+1, last)
	}
}

type Compare func(middle int) int

func BinarySearchAny(len int, order int, compare Compare) int {
	if len == 0 {
		return -1
	}

	n := -1
	switch order {
	case datax.LittleEndian:
		n, _, _ = LittleBinarySearchAny(0, len-1, compare)
	case datax.BigEndian:
		n, _, _ = BigBinarySearchAny(0, len-1, compare)
	}
	return n
}

// littleBinarySearchAny
//
//	@Description: 二分查找元素，基于给定的compare方法比较查找目标元素位置 | 从小到大的顺序
//	@param idx 开始坐标
//	@param last 结束坐标
//	@param compare 比较方法
//	@return n 元素位置
//	@return left 查找结束位置 - 左边
//	@return right 查找结束位置 - 右边
func LittleBinarySearchAny(idx int, last int, compare Compare) (n int, left int, right int) {
	// 终止条件
	if idx > last {
		return -1, last, idx
	}
	// 中位
	middle := (idx + last) / 2
	// 比较结果 | 中位 与 目标值比较大小
	result := compare(middle)
	// 处理
	switch result {
	case datax.EQ:
		// 等于
		return middle, idx, last
	case datax.GT:
		// 大于
		return LittleBinarySearchAny(idx, middle-1, compare)
	case datax.LT:
		// 小于
		return LittleBinarySearchAny(middle+1, last, compare)
	}

	return -1, 0, 0
}

// bigBinarySearchAny 二分查找元素，基于给定的compare方法比较查找目标元素位置 | 从大到小的顺序排序
//
//	@param idx 开始坐标
//	@param last 结束坐标
//	@param compare 比较方法
//	@return n 元素位置
//	@return left 查找结束位置 - 左边
//	@return right 查找结束位置 - 右边
func BigBinarySearchAny(idx int, last int, compare Compare) (n int, left int, right int) {
	// 终止条件
	if idx < last {
		return -1, idx, last
	}
	// 中位
	middle := (idx + last) / 2
	// 比较结果 | 中位 与 目标值比较大小
	result := compare(middle)
	// 处理
	switch result {
	case datax.EQ:
		// 等于
		return middle, idx, last
	case datax.GT:
		// 大于
		return BigBinarySearchAny(middle+1, last, compare)
	case datax.LT:
		// 小于
		return BigBinarySearchAny(idx, middle-1, compare)
	}
	return -1, 0, 0
}

type SearchRangeHandler func(idx int) (notBreakUp bool)

// LittleBinarySearchRangeAny
//
//	@Description: 范围查找处理 -- 通过二分查找的方式获取到目标元素范围并进行处理，自定义验证范围方式及处理
func BinarySearchRangeAny(len int, order int, compare Compare, hander SearchRangeHandler) int {
	// 获取到目标元素的范围内
	n := BinarySearchAny(len, order, compare)
	if n == -1 {
		return n
	}
	// 左右开弓读取元素处理
	if !hander(n) {
		return n
	}

	// 处理左边
	for i := n - 1; i >= 0; i-- {
		if compare(i) != datax.EQ {
			break
		}
		if !hander(i) {
			return n
		}
	}
	// 处理右边
	for i := n + 1; i < len; i++ {
		if compare(i) != datax.EQ {
			break
		}
		if !hander(i) {
			return n
		}
	}
	return n
}

func LinaryBinarySearchRangeAny(len int, compare Compare, hander SearchRangeHandler) int {
	return BinarySearchRangeAny(len, datax.LittleEndian, compare, hander)
}

func BigBinarySearchRangeAny(len int, compare Compare, hander SearchRangeHandler) int {
	return BinarySearchRangeAny(len, datax.BigEndian, compare, hander)
}
