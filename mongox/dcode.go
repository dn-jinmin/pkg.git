/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package mongox

import "go.mongodb.org/mongo-driver/bson"

// map[string]string to bson.D
func MapValueStringDecodeD(datas map[string]string) bson.D {
	res := make(bson.D, 0, len(datas))
	for k, v := range datas {
		res = append(res, bson.E{
			Key:   k,
			Value: v,
		})
	}
	return res
}

// []map[string]string to []bson.D
func MapValueStringDecodeDs(datas []map[string]string) []bson.D {
	res := make([]bson.D, 0, len(datas))

	for _, v := range datas {
		res = append(res, MapValueStringDecodeD(v))
	}

	return res
}

// map[string]string to documents
func MapValueStringDecodeDocuments(datas []map[string]string) []interface{} {
	docs := make([]interface{}, 0, len(datas))

	for _, v := range datas {
		docs = append(docs, v)
	}

	return docs
}
