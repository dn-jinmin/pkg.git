/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package mongox

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsoncodec"
	"go.mongodb.org/mongo-driver/bson/bsonoptions"
	"log"
	"reflect"
	"time"

	"github.com/zeromicro/go-zero/core/breaker"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var primitiveCodecs bson.PrimitiveCodecs

var tTime = reflect.TypeOf(time.Time{})

type Model struct {
	client     *mongo.Client
	db         *mongo.Database
	Collection *mongo.Collection
	brk        breaker.Breaker
}

// MustNewModel returns a Model, exits on errors.
func MustNewModel(url, dbStr, collectionStr string) *Model {
	model, err := NewModel(url, dbStr, collectionStr)
	if err != nil {
		log.Fatal(err)
	}
	return model
}

// NewModel returns a Model.
func NewModel(url, dbStr, collectionStr string) (*Model, error) {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(url), &options.ClientOptions{
		// mongodb 默认对时间采用isodata 是utc的时间，需要自定义不做转化
		Registry: NewRegistryBuilder().Build(),
	})

	if err != nil {
		return nil, err
	}

	var (
		db         = client.Database(dbStr)
		collection = db.Collection(collectionStr)
	)

	return &Model{
		client:     client,
		db:         db,
		Collection: collection,
		brk:        breaker.GetBreaker(url),
	}, err
}

func NewRegistryBuilder() *bsoncodec.RegistryBuilder {
	rb := bsoncodec.NewRegistryBuilder()
	bsoncodec.DefaultValueEncoders{}.RegisterDefaultEncoders(rb)
	bsoncodec.DefaultValueDecoders{}.RegisterDefaultDecoders(rb)
	primitiveCodecs.RegisterPrimitiveCodecs(rb)

	rb.RegisterTypeDecoder(tTime, bsoncodec.NewTimeCodec(bsonoptions.TimeCodec().SetUseLocalTimeZone(true)))

	return rb
}

// InsertOne inserts docs into mm.
func (m *Model) InsertOne(document interface{},
	opts ...Option) (*mongo.InsertOneResult, error) {
	return m.InsertOneCtx(context.Background(), document, opts...)
}

// InsertOneCtx inserts docs into mm.
func (m *Model) InsertOneCtx(ctx context.Context, document interface{},
	opts ...Option) (res *mongo.InsertOneResult, err error) {
	err = m.brk.DoWithAcceptable(func() error {
		opt := defaultOptions(opts...)
		res, err = m.Collection.InsertOne(ctx, document, opt.insertOneOptions...)
		return err
	}, acceptable)
	return
}

// InsertMany
func (m *Model) InsertMany(documents []interface{},
	opts ...Option) (*mongo.InsertManyResult, error) {
	return m.InsertManyCtx(context.Background(), documents, opts...)
}

// InsertManyCtx
func (m *Model) InsertManyCtx(ctx context.Context, documents []interface{},
	opts ...Option) (res *mongo.InsertManyResult, err error) {
	err = m.brk.DoWithAcceptable(func() error {
		opt := defaultOptions(opts...)
		res, err = m.Collection.InsertMany(ctx, documents, opt.insertManyOptions...)
		return err
	}, acceptable)
	return
}

// UpdateOne
func (m *Model) UpdateOne(filter interface{}, update interface{},
	opts ...Option) (*mongo.UpdateResult, error) {
	return m.UpdateOneCtx(context.Background(), filter, update, opts...)
}

// UpdateOneCtx
func (m *Model) UpdateOneCtx(ctx context.Context, filter interface{}, update interface{},
	opts ...Option) (res *mongo.UpdateResult, err error) {
	err = m.brk.DoWithAcceptable(func() error {
		opt := defaultOptions(opts...)
		res, err = m.Collection.UpdateOne(ctx, filter, update, opt.updataOneOptions...)
		return err
	}, acceptable)
	return
}

// DeleteMany
func (m *Model) DeleteMany(filter interface{}, opts ...Option) (res *mongo.DeleteResult, err error) {
	return m.DeleteManyCtx(context.Background(), filter, opts...)
}

func (m *Model) DeleteManyCtx(ctx context.Context, filter interface{}, opts ...Option) (res *mongo.DeleteResult, err error) {
	err = m.brk.DoWithAcceptable(func() error {
		opt := defaultOptions(opts...)
		res, err = m.Collection.DeleteMany(ctx, filter, opt.deleteOptions...)
		return err
	}, acceptable)
	return
}

// DeleteOne
func (m *Model) DeleteOne(filter interface{}, opts ...Option) (res *mongo.DeleteResult, err error) {
	return m.DeleteOneCtx(context.Background(), filter, opts...)
}

func (m *Model) DeleteOneCtx(ctx context.Context, filter interface{}, opts ...Option) (res *mongo.DeleteResult, err error) {
	err = m.brk.DoWithAcceptable(func() error {
		opt := defaultOptions(opts...)
		res, err = m.Collection.DeleteOne(ctx, filter, opt.deleteOptions...)
		return err
	}, acceptable)
	return
}

// find
func (m *Model) Find(filter interface{}, opts ...Option) (cur *mongo.Cursor, err error) {
	return m.FindCtx(context.Background(), filter, opts...)
}

func (m *Model) FindCtx(ctx context.Context, filter interface{}, opts ...Option) (cur *mongo.Cursor, err error) {
	err = m.brk.DoWithAcceptable(func() error {
		opt := defaultOptions(opts...)
		cur, err = m.Collection.Find(ctx, filter, opt.findOptions...)
		return err
	}, acceptable)
	return
}

// findOne
func (m *Model) FindOne(filter interface{}, opts ...Option) (*mongo.SingleResult, error) {
	return m.FindOneCtx(context.Background(), filter, opts...)
}

func (m *Model) FindOneCtx(ctx context.Context, filter interface{},
	opts ...Option) (res *mongo.SingleResult, err error) {
	err = m.brk.DoWithAcceptable(func() error {
		opt := defaultOptions(opts...)
		res = m.Collection.FindOne(ctx, filter, opt.findOneOptions...)
		return nil
	}, acceptable)
	return
}

func acceptable(err error) bool {
	return err == nil || err == context.Canceled
}
