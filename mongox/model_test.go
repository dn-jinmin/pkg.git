/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package mongox

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"testing"
	"time"
)

type Test struct {
	Id       string
	Name     string
	List     []string
	Birthday time.Time
}

func getModel() *Model {
	return MustNewModel("mongodb://192.168.145.10:27017", "oa", "user")
}

func TestModel_Insert(t *testing.T) {
	now := time.Now()
	t.Log(now)
	data := Test{
		Id:       "oa22",
		Name:     "oa22",
		List:     []string{"oa22"},
		Birthday: now,
	}
	t.Log(data)
	model := getModel()

	insertRes, err := model.InsertOne(&data)
	t.Logf("insert one id = %s, res = %v , err = %v", data.Id, insertRes, err)

}

func TestModel_Update(t *testing.T) {
	model := getModel()
	id := "oa22"
	filter := bson.D{{"id", id}}
	updateName := bson.D{
		{
			"$set", bson.D{{"name", "99999"}},
		},
	}
	updateRes, err := model.UpdateOne(filter, updateName)
	t.Logf("update one id = %s, res = %v , err = %v", id, updateRes, err)
}

func TestModel_Find(t *testing.T) {
	model := getModel()
	cur, err := model.Find(bson.D{{"id", "666"}})
	if err != nil {
		t.Log(err)
		return
	}
	defer cur.Close(context.Background())

	//loc, err := time.LoadLocation("Local")
	var data Test
	for cur.Next(context.Background()) {
		t.Log(cur.Current)
		t.Log(cur.Decode(&data))
		t.Log(data)

		//t.Log(data.Birthday.In(loc))
	}

}

type Manager struct {
	Id            primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Phone         string             `json:"phone" bson:"phone"`
	Email         string             `json:"email" bson:"email"`
	Name          string             `json:"name" bson:"name"`
	Role          []int64            `json:"role" bson:"role"`
	Status        uint8              `json:"status" bson:"status"`
	DepartmentId  []string           `json:"department_id" bson:"department_id"` //组织id
	Password      string             `json:"-" bson:"password"`
	LastLoginTime int64              `json:"last_login_time" bson:"last_login_time"`
	LastLoginIp   string             `json:"last_login_ip" bson:"last_login_ip"`
	CreatedAt     int64              `json:"created_at" bson:"created_at"`
	UpdatedAt     int64              `json:"updated_at" bson:"updated_at"`
	FaceId        string             `json:"face_id" bson:"face_id,omitempty"`
	FaceImage     string             `json:"face_image" bson:"face_image,omitempty"`
	OperateName   string             `json:"operate_name" bson:"operate_name,omitempty"`
	Url           string             `json:"url" bson:"url"` //草稿地址
}

func TestManagerDevManger_insert(t *testing.T) {
	model := MustNewModel("mongodb://192.168.145.10:27017", "management_dev", "manager")
	now := time.Now()
	data := Manager{
		Phone:         "1311888222",
		Email:         "13188812222@163.com",
		Name:          "6688866",
		Role:          nil,
		Status:        1,
		DepartmentId:  []string{"5faba51af8be71baa902663f"},
		Password:      "1311110000",
		LastLoginTime: now.Unix(),
		LastLoginIp:   "127.0.0.1",
		CreatedAt:     now.Unix(),
		UpdatedAt:     now.Unix(),
		FaceId:        "",
		FaceImage:     "",
		OperateName:   "",
		Url:           "xxx/1.png",
	}

	insertRes, err := model.InsertOne(&data)
	t.Logf("insert one id = %s, res = %v , err = %v", data.Id, insertRes, err)
}

func TestManagerDevManger_updateOne(t *testing.T) {
	model := MustNewModel("mongodb://192.168.145.10:27017", "management_dev", "manager")

	type args struct {
		query  bson.D
		update bson.D
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"id修改", args{
			query: bson.D{{"_id", ObjectIDFromHex("64213f3fe30e9659aa18773e")}},
			update: bson.D{
				{
					"$set", bson.D{{"name", "999009"}},
				},
			},
		}, false},
		{"status修改", args{
			query: bson.D{{"status", 1}},
			update: bson.D{
				{
					"$set", bson.D{{"name", "1111"}},
				},
			},
		}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			updateRes, err := model.UpdateOne(tt.args.query, tt.args.update)
			t.Logf("update one  res = %v , err = %v", updateRes, err)
		})
	}
}

func TestManagerDevManger_deleteOne(t *testing.T) {
	model := MustNewModel("mongodb://192.168.145.10:27017", "management_dev", "manager")
	type args struct {
		query bson.D
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"删除", args{
			query: bson.D{{"status", 1}},
		}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			deleteRes, err := model.DeleteMany(tt.args.query)
			t.Logf("delete one res = %v, err = %v", deleteRes, err)
		})
	}
}

type StaffContactRecord struct {
	Id        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	UserId    string             `json:"user_id" bson:"user_id,omitempty"`       // 用户id
	TargetId  string             `json:"target_id" bson:"target_id,omitempty"`   // 联系用户id
	CompanyId string             `json:"company_id" bson:"company_id,omitempty"` // 公司id
	CreatedAt int64              `json:"created_at" bson:"created_at,omitempty"` // 创建时间
	UpdatedAt int64              `json:"updated_at" bson:"updated_at,omitempty"` // 更新时间
	Count     int64              `json:"-" bson:"count,omitempty"`               // 联系次数
}

func TestMongoAggregate(t *testing.T) {
	model := MustNewModel("mongodb://root:oa123456@175.6.34.53:47017", "oa", "staff_contact_record")
	cur, err := model.Collection.Aggregate(context.TODO(), mongo.Pipeline{
		bson.D{
			{"$group", bson.D{
				{"_id", "$target_id"},
			}},
		},
		bson.D{
			{"$match", bson.D{
				{"user_id", "64127310452e6735f73db246"},
			}},
		},
	}, &options.AggregateOptions{})
	list := make([]StaffContactRecord, 0)
	err = cur.All(context.Background(), &list)
	t.Log(err)
	t.Log(list)
}
