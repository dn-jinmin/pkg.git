/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package mongox

import (
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	defaultTimeout = time.Second
)

type (
	option struct {
		timeout time.Duration

		insertOneOptions  []*options.InsertOneOptions
		insertManyOptions []*options.InsertManyOptions
		updataOneOptions  []*options.UpdateOptions
		deleteOptions     []*options.DeleteOptions
		findOptions       []*options.FindOptions
		findOneOptions    []*options.FindOneOptions
	}

	Option func(opts *option)
)

func defaultOptions(opts ...Option) *option {

	opt := &option{
		timeout: defaultTimeout,
	}

	for _, o := range opts {
		o(opt)
	}
	return opt
}

func WithTimeout(timeout time.Duration) Option {
	return func(opts *option) {
		opts.timeout = timeout
	}
}
func WithInsertOneOptions(bypassDocumentValidation bool, comment interface{}) Option {
	return func(opts *option) {
		o := options.InsertOne()
		o.SetComment(comment)
		o.SetBypassDocumentValidation(bypassDocumentValidation)
		opts.insertOneOptions = append(opts.insertOneOptions, o)
	}
}

func WithInsertManyOptions(bypassDocumentValidation bool, comment interface{}, ordered bool) Option {
	return func(opts *option) {
		opts.insertManyOptions = append(opts.insertManyOptions, &options.InsertManyOptions{
			BypassDocumentValidation: &bypassDocumentValidation,
			Comment:                  comment,
			Ordered:                  &ordered,
		})
	}
}

func WithUpdateOneOptions(updateOneOption ...*options.UpdateOptions) Option {
	return func(opts *option) {
		opts.updataOneOptions = append(opts.updataOneOptions, updateOneOption...)
	}
}
func WithDeleteOptions(deleteOption ...*options.DeleteOptions) Option {
	return func(opts *option) {
		opts.deleteOptions = append(opts.deleteOptions, deleteOption...)
	}
}

func WithFindOptions(findOptions ...*options.FindOptions) Option {
	return func(opts *option) {
		opts.findOptions = append(opts.findOptions, findOptions...)
	}
}
