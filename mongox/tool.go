/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package mongox

import (
	"github.com/globalsign/mgo/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func ByIdResBsonDocElem(id interface{}) bson.DocElem {
	return bson.DocElem{"id", id}
}

func ObjectIDFromHex(s string) primitive.ObjectID {
	objectId, _ := primitive.ObjectIDFromHex(s)

	return objectId
}
