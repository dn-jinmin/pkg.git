/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package tool

import (
	"fmt"
	"strings"
)

func ImplodeInt64(separator string, ids []int64) string {
	return strings.Replace(strings.Trim(fmt.Sprint(ids), "[]"), " ", separator, -1)
}
