/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */

package tool

import (
	"strconv"
	"time"
)

var DefaultTimeLayout = "2006-01-02 15:04:05"

func StringToTime(str, timeLayout string) time.Time {
	local, _ := time.LoadLocation("Asia/Shanghai")
	data, err := time.ParseInLocation(timeLayout, str, local)
	if err != nil {
		return time.Time{}
	}
	return data
}

func StringToInt64(data string) int64 {
	d, err := strconv.Atoi(data)
	if err != nil {
		return 0
	}
	return int64(d)
}

func StringToInt(str string) int {
	d, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}
	return d
}
