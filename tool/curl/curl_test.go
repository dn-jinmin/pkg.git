/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package curl

import (
	"testing"
)

func TestPost(t *testing.T) {

	req := `{"uid":1, "seq":2}`

	type args struct {
		api  string
		body []byte
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"1", args{
				api:  "http://127.0.0.1:8888/v1/chat/inform",
				body: []byte(req),
			}, false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Post(tt.args.api, tt.args.body)
			if (err != nil) != tt.wantErr {
				t.Errorf("Post() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Log(string(got))
		})
	}
}
