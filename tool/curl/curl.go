package curl

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

func Get(api string) ([]byte, error) {

	// 发起请求

	req, err := http.NewRequest("GET", api, nil)
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// 处理参数

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return body, nil

}

//func Post(api string, body []byte) ([]byte, error) {
//	req, err := http.NewRequest("post", api, bytes.NewBuffer(body))
//	if err != nil {
//		return nil, err
//	}
//	req.Header.Set("Content-Type", "application/json")
//	client := &http.Client{}
//
//	resp, err := client.Do(req)
//
//	if err != nil {
//		return nil, err
//	}
//	defer resp.Body.Close()
//
//	return ioutil.ReadAll(resp.Body)
//}

func Post(api string, b []byte) ([]byte, error) {
	body := bytes.NewBuffer(b)
	resp, err := http.Post(api, "application/json;charset=utf-8", body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}
