module gitee.com/dn-jinmin/pkg/tool

go 1.18

require (
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/valyala/fastrand v1.1.0
	github.com/zeromicro/go-zero v1.5.0
	go.uber.org/zap v1.24.0
	golang.org/x/crypto v0.7.0
)

require (
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/fatih/color v1.14.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/spaolacci/murmur3 v1.1.0 // indirect
	go.opentelemetry.io/otel v1.14.0 // indirect
	go.opentelemetry.io/otel/trace v1.14.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/automaxprocs v1.5.1 // indirect
	go.uber.org/multierr v1.9.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
)
