package tool

import (
	"crypto/md5"
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"
)

// 加密算法

func Md5(str []byte) string {
	h := md5.New()
	h.Write(str)
	return hex.EncodeToString(h.Sum(nil))
}

// hash加密
func GenPasswordHash(password []byte) ([]byte, error) {
	return bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
}
// hash校验
func ValidatePasswordHash(password []byte, hashed string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashed), password); err != nil {
		return false
	}
	return true
}
