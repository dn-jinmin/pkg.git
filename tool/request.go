/*
 * @auther: dn-jinmin/dn-jinmin
 * @desc:
 */

package tool

import (
	"net"
	"net/http"
)

func GetRemoteClientIp(r *http.Request) string {
	remoteIp := r.RemoteAddr

	if ip := r.Header.Get("X-Real-IP"); ip != "" {
		remoteIp = ip
	} else if ip = r.Header.Get("X-Forwarded-For"); ip != "" {
		remoteIp = ip
	} else {
		remoteIp, _, _ = net.SplitHostPort(remoteIp)
	}

	if remoteIp == "::1" {
		remoteIp = "127.0.0.1"
	}

	return remoteIp
}

func Platform(r *http.Request) string {
	var (
		pc 		= "pc"
		h5 		= "h5"
		mini 	= "mini"
		app 	= "app"
	)

	userAgent := r.UserAgent()

	if len(userAgent) == 0{
		return pc
	}

	switch userAgent {
	case "Mobile":
		return h5
	case "Android":
		return app
	case "Silk/":

	case "Kindle":

	case "BlackBerry", "Opera Mini", "Opera Mobi":
		return mini
	}
	return pc
}