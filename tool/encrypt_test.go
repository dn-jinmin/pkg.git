package tool

import (
	"testing"
)

func TestGenPasswordHash(t *testing.T) {
	type args struct {
		password string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"1", args{password: "0000"}, false},
		{"2", args{password: "0000"}, false},
		{"3", args{password: "1111"}, false},
		{"4", args{password: "1111"}, false},
		{"5", args{password: "2222"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GenPasswordHash([]byte(tt.args.password))
			if (err != nil) != tt.wantErr {
				t.Errorf("GenPasswordHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			t.Log(ValidatePasswordHash([]byte("0000"), "$2a$10$53jVZk0VBS8O9va8m6YWoO6IwQZMEf.HNAODudF6ByZ.nVntmVDdK"))
			t.Logf("name := %s, pwd := %s, got := %s", tt.name, tt.args.password, string(got))
		})
	}
}

func TestMd5(t *testing.T) {
	type args struct {
		str []byte
	}
	tests := []struct {
		name string
		args args
	}{
		{"1", args{str: []byte("cloud.edu")}},
		{"2", args{str: []byte("cloud.edu")}},
		{"3", args{str: []byte("www.jinmin.com")}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Md5(tt.args.str)
			t.Logf("name %s, str %v, got %v", tt.name, string(tt.args.str), got)
		})
	}
}
