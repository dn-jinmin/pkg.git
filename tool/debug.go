/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package tool

import "github.com/zeromicro/go-zero/core/logx"

func Debugf(prefix, format string, v ...interface{}) {
	var vt []interface{}
	vt = append(vt, prefix)
	vt = append(vt, v...)
	logx.Debugf("【%s】"+format, vt...)
}

func DebugE(prefix, format string, err error, v ...interface{}) {
	if err != nil {
		var vt []interface{}
		vt = append(vt, prefix, err)
		vt = append(vt, v...)
		logx.Debugf("【%s】"+format, vt...)
	}
}
func Debug(prefix, desc string, DDs ...func(prefix string)) {
	Debugf(prefix, desc+" ========= start ===========")
	for _, d := range DDs {
		d(prefix)
	}
	Debugf(prefix, desc+" ========= end ===========")
}

func DD(format string, v ...interface{}) func(prefix string) {
	return func(prefix string) {
		Debugf(prefix, format, v...)
	}
}
func DE(format string, err error, v ...interface{}) func(prefix string) {
	return func(prefix string) {
		DebugE(prefix, format, err, v...)
	}
}
