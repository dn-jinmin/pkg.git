package tool

import (
	"github.com/valyala/fastrand"
)

var (
	rngFast fastrand.RNG
)

func RInt(hint int) int {
	return int(rngFast.Uint32n(uint32(hint)))
}