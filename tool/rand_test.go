package tool

import "testing"

func TestRInt(t *testing.T) {
	type args struct {
		hint int
	}
	tests := []struct {
		name string
		args args
	}{
		{"1", args{hint: 8999}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i := 0; i < 10; i++ {
				got := RInt(tt.args.hint) + 1000
				t.Logf("name : %s, hint : %d, got : %d", tt.name, tt.args.hint, got)
			}
		})
	}
}