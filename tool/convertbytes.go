package tool

import (
	"bytes"
	"encoding/binary"
	"errors"
)

var (
	errorBytesToIntLengthInvaild = errors.New("bytes to int bytes length is invaild")
)

func BytesToInt(b []byte, isSymbol bool) (int, error) {

	if isSymbol {
		return bytesToIntS(b)
	}
	return bytesToIntU(b)
}

//
// 字节数(大端)组成转int(无序)
//
func bytesToIntU(b []byte) (int, error) {
	if len(b) == 3 {
		b = append([]byte{0}, b...)
	}

	bytesBuffer := bytes.NewBuffer(b)
	switch len(b) {
	case 1:
		var tmp uint8
		err := binary.Read(bytesBuffer, binary.BigEndian, &tmp)
		return int(tmp), err
	case 2:
		var tmp uint16
		err := binary.Read(bytesBuffer, binary.BigEndian, &tmp)
		return int(tmp), err
	case 4:
		var tmp uint32
		err := binary.Read(bytesBuffer, binary.BigEndian, &tmp)
		return int(tmp), err
	default:
		return 0, errorBytesToIntLengthInvaild
	}
}

//
// 字节数(大端)组成转int(有序)
//
func bytesToIntS(b []byte) (int, error) {
	if len(b) == 3 {
		b = append([]byte{0}, b...)
	}

	bytesBuffer := bytes.NewBuffer(b)
	switch len(b) {
	case 1:
		var tmp int8
		err := binary.Read(bytesBuffer, binary.BigEndian, &tmp)
		return int(tmp), err
	case 2:
		var tmp int16
		err := binary.Read(bytesBuffer, binary.BigEndian, &tmp)
		return int(tmp), err
	case 4:
		var tmp int
		err := binary.Read(bytesBuffer, binary.BigEndian, &tmp)
		return tmp, err
	default:
		return 0, errorBytesToIntLengthInvaild
	}
}
