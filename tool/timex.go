/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package tool

import (
	"log"
	"time"
)

var timeLoadLocation *time.Location

func init() {
	loc, err := time.LoadLocation("Local")
	if err != nil {
		log.Fatal("time loadLocation err", err)
	}
	timeLoadLocation = loc
}

func TimeToCST(t time.Time) time.Time {
	return t.In(timeLoadLocation)
}
