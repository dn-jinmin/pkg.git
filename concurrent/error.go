/**
 * @Author: dn-jinmin
 * @File:  concurrent
 * @Version: 1.0.0
 * @Date: 2023/3/17
 * @Description:
 */

package concurrent

import "strings"

type ConcurrentError []error

func NewConcurrentError(ln int) ConcurrentError {
	return make([]error, 0, ln)
}

func (ce ConcurrentError) Error() string {
	var res strings.Builder
	for i, e := range ce {
		res.WriteString(e.Error())
		if i < len(ce)-1 {
			res.WriteString("|")
		}
	}
	return res.String()
}
