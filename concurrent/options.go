/**
 * @Author: dn-jinmin
 * @File:  concurrent
 * @Version: 1.0.0
 * @Date: 2023/3/17
 * @Description: 并发执行操作属性
 */

package concurrent

import "time"

const (
	DefaultErrorSkip        = false
	DefaultErrorConcurrency = 5
	DefaultTimeout          = 1 * time.Second
	DefaultErrorOutAll      = false
)

type (
	Options func(opt *options)

	options struct {
		// 超时时间
		timeout time.Duration
		// 并发量
		concurrency int
		// 错误忽视
		errorSkip bool
		// 输出所有错误
		errorOutAll bool
	}
)

func NewOption(opts ...Options) *options {
	opt := &options{
		timeout:     DefaultTimeout,
		errorSkip:   DefaultErrorSkip,
		concurrency: DefaultErrorConcurrency,
		errorOutAll: DefaultErrorOutAll,
	}

	for _, f := range opts {
		f(opt)
	}

	return opt
}

// 设置超时
func WithTimeout(timeout time.Duration) Options {
	return func(opt *options) {
		opt.timeout = timeout
	}
}

// 设置错误跳过处理
func WithErrorSkip(errorSkip bool) Options {
	return func(opt *options) {
		opt.errorSkip = errorSkip
	}
}

// 设置并发量
func WithConcurrency(concurrency int) Options {
	return func(opt *options) {
		if concurrency == 0 {
			concurrency = DefaultErrorConcurrency
		}
		opt.concurrency = concurrency
	}
}

func WithErrorOutAll(errorOutAll bool) Options {
	return func(opt *options) {
		opt.errorOutAll = errorOutAll
	}
}
