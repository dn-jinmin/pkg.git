/**
 * @Author: dn-jinmin
 * @File:  utils
 * @Version: 1.0.0
 * @Date: 2023/3/17
 * @Description: 并发执行：应用于多个不相干的请求通过协程并发执行
 */

package concurrent

import (
	"context"
	"errors"
	"go.uber.org/atomic"
)

var TimeoutErr = errors.New("请求超时")

type ConcurrentHandler func(ctx context.Context) (err error)

func Handler(ctx context.Context, opt *options, cf ...ConcurrentHandler) (err error) {

	if opt == nil {
		opt = NewOption()
	}
	// 利用 channel 控制并发量
	concurrency := make(chan struct{}, opt.concurrency)
	errCh := make(chan error, len(cf))
	rerr := NewConcurrentError(len(cf))

	// 是否设置超时
	_, ok := ctx.Deadline()
	if !ok {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, opt.timeout)
		defer cancel()
	}

	// 计算执行次数用于退出程序
	execCount := atomic.NewInt32(0)

	// 并发执行方法
	go func() {
		for _, handler := range cf {
			concurrency <- struct{}{}
			go func(ctx context.Context, f ConcurrentHandler) {
				// 结束当前协程， 释放通道
				defer func() {
					<-concurrency
				}()
				// 执行方法
				select {
				case errCh <- f(ctx):
				case <-ctx.Done():
				}
			}(ctx, handler)
		}
	}()

	defer func() {
		if len(rerr) == 0 {
			err = nil
		}
		if len(rerr) == 1 || (len(rerr) > 0 && !opt.errorOutAll) {
			err = rerr[0]
		}
	}()

	// 控制程序处理及退出
	for {
		select {
		case <-ctx.Done():
			rerr = append(rerr, TimeoutErr)
			// 外层程序控制退出
			return rerr
		case err := <-errCh:
			if err != nil {
				rerr = append(rerr, err)
				// 执行遇错是否退出
				if !opt.errorSkip {
					return rerr
				}
			}
			execCount.Inc()
			if execCount.Load() >= int32(len(cf)) {
				return rerr
			}
		}
	}
}
