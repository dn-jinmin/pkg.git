/**
 * @Author: dn-jinmin
 * @File:  concurrent_test
 * @Version: 1.0.0
 * @Date: 2023/3/17
 * @Description:
 */

package concurrent

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"runtime/trace"
	"testing"
	"time"
)

func TestHandler(t *testing.T) {
	type args struct {
		ctx context.Context
		cf  []ConcurrentHandler
	}
	tests := []struct {
		name string
		args args
	}{
		{"1", args{
			ctx: context.Background(),
			cf: []ConcurrentHandler{
				func(ctx context.Context) error {
					return nil
				},
				func(ctx context.Context) error {
					return errors.New("go 2 err")
				},
				func(ctx context.Context) error {
					return errors.New("go 3 err")
				},
			},
		}},
		{"从mysql与rpc中获取数据测试", args{
			ctx: context.Background(),
			cf:  []ConcurrentHandler{},
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := Handler(tt.args.ctx, nil, tt.args.cf...)
			t.Log(err)
		})
	}
}

func ExampleHandler() {
	f, _ := os.Create("cur.out")
	defer f.Close()
	// 启动trace 对程序的执行进行分析捕捉
	if err := trace.Start(f); err != nil {
		panic(err)
	}
	defer trace.Stop()

	var (
		uid = 1
		rid = 2
		key = "user:list"
	)

	var (
		userInfo string
		roleInfo string
		ids      []string
	)

	options := NewOption(
		WithTimeout(4*time.Second),
		WithErrorSkip(false),
		WithConcurrency(1),
	)

	err := Handler(context.Background(), options,
		func(ctx context.Context) (err error) {
			userInfo, err = sourceMysqlGetUser(uid)
			return errors.New("测试")
		}, func(ctx context.Context) (err error) {
			roleInfo, err = sourceGrpcGetRole(rid)
			return errors.New("测试")

		}, func(ctx context.Context) (err error) {
			ids, err = sourceRedisGrpcIds(key)
			return errors.New("测试")

		})

	if err != nil {
		log.Fatal(err.Error())
	}
	log.Println("userInfo : ", userInfo, " roleInfo ", roleInfo, " ids ", ids)
}

func BenchmarkHandlerParam(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var (
			uid = 1
			rid = 2
			key = "user:list"
		)

		var (
			userInfo string
			roleInfo string
			ids      []string
		)

		err := Handler(context.Background(), nil, func(ctx context.Context) (err error) {
			userInfo, err = sourceMysqlGetUser(uid)
			return
		}, func(ctx context.Context) (err error) {
			roleInfo, err = sourceGrpcGetRole(rid)
			return
		}, func(ctx context.Context) (err error) {
			ids, err = sourceRedisGrpcIds(key)
			return
		})
		b.Log(err, userInfo, roleInfo, ids)
	}
}

func sourceMysqlGetUser(uid int) (string, error) {
	time.Sleep(1 * time.Second)
	return fmt.Sprintf("uid : %d", uid), nil
}

func sourceGrpcGetRole(rid int) (string, error) {
	time.Sleep(2 * time.Second)
	return fmt.Sprintf("role : %d", rid), errors.New("sourceGrpcGetRole 请求超时")
}
func sourceRedisGrpcIds(key string) ([]string, error) {
	return []string{"1", "2", "3"}, nil
}
