package jwtx

import "github.com/golang-jwt/jwt"

const Identify = "uid"

func GetToken(secretKey string, iat, seconds, uid int64) (string, error)   {
	claims := make(jwt.MapClaims)
	claims["exp"] = iat + seconds
	claims["iat"] = iat
	claims[Identify] = uid
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = claims
	return token.SignedString([]byte(secretKey))
}