/**
 * @Author: dn-jinmin
 * @File:  chatgpt
 * @Version: 1.0.0
 * @Date: 2023/4/19
 * @Description: 根据chatgpt的特点设计发送信息的模板，以符合chatgpt调用中的数据生成
 */

package chatgpt

import (
	"context"
	"fmt"
	"github.com/sashabaranov/go-openai"
	"strings"
)

const (
	CopywriterMarkBody = iota
	CopywriterMarkDetail
)

// 文案类型的模板
// 应用于文章，论文，方案等场景设计
type Copywriter struct {
	*openai.Client

	// openai的model
	model string
	// MaxTokens
	maxTokens int
	// stream
	stream bool
}

func NewCopywriter(c *openai.Client) *Copywriter {
	return &Copywriter{
		Client: c,

		// 默认属性
		model:     openai.GPT3TextDavinci003,
		maxTokens: 900,
		stream:    false,
	}
}

// 生成内容
func (t *Copywriter) Generate(ctx context.Context, content string, opt *option) (string, error) {
	// 合并
	t.withOptions(opt)

	req := openai.CompletionRequest{
		Model:     opt.model,
		Prompt:    content,
		MaxTokens: opt.maxTokens,
		Stream:    opt.stream,
	}

	// 不走模板
	if !opt.template {
		res, err := t.createCompletion(ctx, req, opt)
		if err != nil {
			return "", nil
		}
		// 结束
		return res, nil
	}

	// 走模板必须采用opt.stream, 输出内容为2048字数
	if !opt.stream {
		return "", ErrTemplateNoUseStream
	}
	opt.maxTokens = 4096

	return "", nil
}

// 合并option信息
func (t *Copywriter) withOptions(opt *option) {
	if opt.maxTokens == 0 {
		opt.maxTokens = t.maxTokens
	}

	if opt.model == "" {
		opt.model = t.model
	}
}

// 请求
func (t *Copywriter) createCompletion(ctx context.Context, req openai.CompletionRequest, opt *option) (string, error) {
	var res string

	if opt.stream {
		stream, err := t.Client.CreateCompletionStream(ctx, req)
		if err != nil {
			return "", err
		}
		defer stream.Close()
		res, err = GetStreamContent(stream)
		if err != nil {
			return "", err
		}
	} else {
		response, err := t.Client.CreateCompletion(ctx, req)
		if err != nil {
			return "", err
		}
		res = response.Choices[0].Text
	}
	// 结束
	return res, nil
}

// 发送模板
func (*Copywriter) generateTemplate(mark int, info string) string {

	var (
		resInfo   = fmt.Sprintf("信息: %s\n", info)
		resAskFor = ""
	)

	switch mark {
	case CopywriterMarkBody:
		// 文案主题
		resAskFor = `要求: 1. 根据信息返回主题与主题介绍, 2. 内容可分为几部分每部分多少字, 3. 要求必须以如下格式返回结果
							主题: xxxx
							介绍: xxxx
							总共约：4000字;
							可分为Y部分:
							一、xxx：1000字;
							二、xxx：1000字;`
	case CopywriterMarkDetail:
		// 文案详细
		resAskFor = ` 要求: 1. 内容可分为几段分每段主题以及多少字, 2. 要求必须以如下格式返回结果
							总共约：2000字;
							可分为Y部分:
							第一段, xxx：1000字;
							第一段, xxx：2000字;`
	}

	return fmt.Sprintf(`
				%s
				%s
			`, resInfo, resAskFor)
}

type CopywriterPrompt struct {
	body      string
	Subject   string            // 主题
	Introduce string            // 介绍
	Words     string            // 字数
	Part      map[string]string // 部分内容
}

func ParseCopywriterPrompt(input string) (*CopywriterPrompt, error) {
	cp := &CopywriterPrompt{Part: make(map[string]string)}
	lines := strings.Split(input, "\n")

	for _, line := range lines {

		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		if strings.HasPrefix(line, "主题") {
			cp.Subject = strings.TrimSpace(strings.TrimPrefix(line, "主题: "))
			if cp.Subject == "" {
				cp.Subject = strings.TrimSpace(strings.TrimPrefix(line, "主题："))
			}
		} else if strings.HasPrefix(line, "介绍") {
			cp.Introduce = strings.TrimSpace(strings.TrimPrefix(line, "介绍："))
			if cp.Introduce == "" {
				cp.Introduce = strings.TrimSpace(strings.TrimPrefix(line, "介绍："))
			}
		} else if strings.HasPrefix(line, "总共约") {
			cp.Words = strings.TrimSpace(strings.TrimPrefix(line, "总共约："))
			if cp.Words == "" {
				cp.Words = strings.TrimSpace(strings.TrimPrefix(line, "总共约: "))
			}
		} else if strings.HasPrefix(line, "可分为") {

			//parts := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, "可分为")), ";")
			//for _, part := range parts {
			//	fields := strings.Split(strings.TrimSpace(part), "：")
			//	if len(fields) == 2 {
			//		cp.Part[strings.TrimSpace(fields[0])] = strings.TrimSpace(fields[1])
			//	}
			//}
		}
	}
	return cp, nil
}
