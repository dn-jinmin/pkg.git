/**
 * @Author: dn-jinmin
 * @File:  chatgpt
 * @Version: 1.0.0
 * @Date: 2023/4/19
 * @Description:
 */

package chatgpt

import "context"

type ContentType interface {
	Generate(context context.Context, content string, opt *option) (string, error)
}

const (
	// 文档类型
	CopyWriterTypes = iota
)

//func InitContentTypes(client *openai.Client) {
//	contentTypes := map[int]ContentType{
//		CopyWriterTypes:
//	}
//
//}
