/**
 * @Author: dn-jinmin
 * @File:  chatgpt
 * @Version: 1.0.0
 * @Date: 2023/4/18
 * @Description:
 */

package chatgpt

type Format int

const (
	JsonFormat = iota
)

type (
	Config struct {
		Url string
		Key string
	}

	option struct {
		// openai的model
		model string
		// MaxTokens
		maxTokens int
		// stream
		stream bool
		// 是否走模板
		template bool
	}

	Options func(opt *option)
)

func WithModel(model string) Options {
	return func(opt *option) {
		opt.model = model
	}
}

func WithMaxTokens(maxTokens int) Options {
	return func(opt *option) {
		opt.maxTokens = maxTokens
	}
}

func WithStream(stream bool) Options {
	return func(opt *option) {
		opt.stream = stream
	}
}

func WithTemplate(template bool) Options {
	return func(opt *option) {
		opt.template = template
	}
}