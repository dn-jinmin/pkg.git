/**
 * @Author: dn-jinmin
 * @File:  chatgpt
 * @Version: 1.0.0
 * @Date: 2023/4/19
 * @Description:
 */

package chatgpt

import (
	"errors"
	"io"
	"strings"

	"github.com/sashabaranov/go-openai"
)

// 获取stream中的内容
func GetStreamContent(stream *openai.CompletionStream) (string, error) {
	var res strings.Builder

	for {
		response, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			// 读取完成
			return res.String(), nil
		}

		if err != nil {
			return "", err
		}

		res.WriteString(response.Choices[0].Text)
	}
}
