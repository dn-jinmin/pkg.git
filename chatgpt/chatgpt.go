/**
 * @Author: dn-jinmin
 * @File:  pkg
 * @Version: 1.0.0
 * @Date: 2023/4/18
 * @Description:
 */

package chatgpt

import (
	"context"
	"github.com/sashabaranov/go-openai"
)

// 产出：     用户 -> 主题(内容小) -> 直接发送 -> 拆分获取 -> 组装 -> 输出
// 修改/调整: 用户 -> 内容(内容大) -> 分段发送 -> 拆分获取 -> 组装 -> 输出

type ChatGpt struct {
	*openai.Client

	contentTypes map[int]ContentType
}

func NewChatGpt(cfg *Config) *ChatGpt {
	if cfg.Key == "" {
		panic("option key is null")
	}

	var client *openai.Client
	if cfg.Url == "" {
		client = openai.NewClient(cfg.Key)
	} else {
		aicfg := openai.DefaultConfig(cfg.Key)
		aicfg.BaseURL = cfg.Url
		client = openai.NewClientWithConfig(aicfg)
	}

	contentTypes := map[int]ContentType{
		CopyWriterTypes: NewCopywriter(client),
	}

	return &ChatGpt{
		Client: client,

		contentTypes: contentTypes,
	}
}

//
// Generate
//  @Description: 根据类型生成对应的内容
//  @receiver gpt
//  @param content 内容
//  @param contentType 内容类型
//  @param opt
//  @return string
//  @return error
//
func (gpt *ChatGpt) Generate(context context.Context, content string, contentType int, opts ...Options) (string, error) {

	opt := new(option)
	for _, o := range opts {
		o(opt)
	}

	c, ok := gpt.contentTypes[contentType]
	if ok {
		return c.Generate(context, content, opt)
	}

	return "", nil
}
