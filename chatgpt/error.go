/**
 * @Author: dn-jinmin
 * @File:  chatgpt
 * @Version: 1.0.0
 * @Date: 2023/4/19
 * @Description:
 */

package chatgpt

import "errors"

var (
	ErrTemplateNoUseStream = errors.New("use the stream pattern template")
)
