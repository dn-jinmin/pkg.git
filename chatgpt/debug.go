/**
 * @Author: dn-jinmin
 * @File:  pkg
 * @Version: 1.0.0
 * @Date: 2023/4/19
 * @Description:
 */

package chatgpt

import (
	"bytes"
	"encoding/json"
	"fmt"
)

func JsonPrint(data ...interface{}) {
	for _, datum := range data {
		switch datum.(type) {
		case string, int, int64, int32, uint, uint32, uint64:
			fmt.Println(datum)
		case error:
			fmt.Println("err : ", datum)
		default:
			b, _ := json.Marshal(datum)
			var out bytes.Buffer
			err := json.Indent(&out, b, "", "   ")
			if err != nil {
				return
			}
			fmt.Println(out.String())
		}
	}
}

func DD(desc interface{}, a ...interface{}) {
	if len(a) == 0 {
		fmt.Println("================== >>>>>>>>>> dd >>>>>>>>>>>>> =======")
		JsonPrint(desc)
		fmt.Println("================== <<<<<<<<<< dd <<<<<<<<<<<<< =======")
		return
	}

	fmt.Println("================== >>>>>>>>>> ", desc, " >>>>>>>>>>>>> =======")
	JsonPrint(a...)
	fmt.Println("================== <<<<<<<<<< ", desc, " <<<<<<<<<<<<< =======")
}
