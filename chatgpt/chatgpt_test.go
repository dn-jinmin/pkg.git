/**
 * @Author: dn-jinmin
 * @File:  chatgpt_test
 * @Version: 1.0.0
 * @Date: 2023/4/19
 * @Description:
 */

package chatgpt

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/sashabaranov/go-openai"
)

var chatgpt *ChatGpt

func init() {
	chatgpt = NewChatGpt(&Config{
		Url: "http://154.211.13.247/v1",
		Key: "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",
	})
}

//主题：Java的分布式系统设计
//介绍：随着网络的普及，各类事物的联网，如何动态、有效地处理数据，构建一个有鱼分布式系统，实现协作式办公，节省硬件、软件成本，成为了企业和行业中一项重要的业务。这就要求使用Java语言来设计一个分布式系统，具有高可用性、可扩展性及高可实现性等特点，以满足企业应用的实时需求。本文将通过多方面的技术架构来研究，解决构建分布式系统的问题，建立起一个基于Java的健壮的分布式系统。
//总共约：10000字;
//可分为五部分：
//一、背景介绍：2000字;
//二、技术架构：2000字;
//三、Java编程技术：2500字;
//四、系统构建：2500字;
//五、测试及优化：1500字。
// 利用
func TestCreateCompletionStream1(t *testing.T) {
	type args struct {
		req openai.CompletionRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"openai.GPT3TextDavinci003-1", args{req: openai.CompletionRequest{
			Model: openai.GPT3TextDavinci003,
			Prompt: `信息：编写一篇java毕业论文;
					 要求: 1. 根据信息返回主题与主题介绍, 2. 内容可分为几部分每部分多少字, 3. 要求必须以如下格式返回结果
							主题: xxxx
							介绍: xxxx
							总共约：8000字;
							可分为2部分:
							一、xxx：1000字;
							二、xxx：2000字;
						`,
			MaxTokens: 3096,
			Stream:    true,
		}}, false},
		//{"openai.GPT3TextDavinci003-2", args{req: openai.CompletionRequest{
		//	Model: openai.GPT3TextDavinci003,
		//	Prompt: `信息：主题为Java的分布式系统设计文章分为五部分：一、背景介绍：2000字;二、技术架构：2000字;三、Java编程技术：2500字;四、系统构建：2500字;五、测试及优化：1500字。, 请编写第一部分背景介绍,内容要求字数2000字以上
		//			 要求: 1. 内容可分为几段分每段主题以及多少字, 2. 要求必须以如下格式返回结果
		//					总共约：2000字;
		//					可分为2部分:
		//					第一段, xxx：1000字;
		//					第一段, xxx：2000字;
		//					`,
		//	MaxTokens: 3096,
		//	Stream:    true,
		//}}, false},
		//{"openai.GPT3TextDavinci003-3", args{req: openai.CompletionRequest{
		//	Model: openai.GPT3TextDavinci003,
		//	Prompt: `信息：主题为Java的分布式系统设计文章分为五部分：一、背景介绍：2000字;二、技术架构：2000字;三、Java编程技术：2500字;四、系统构建：2500字;五、测试及优化：1500字；第一部分背景介绍：2000字分为2部分：第一段，分布式系统设计背景介绍:1500字; 第二段，Java分布式系统设计现状及应用介绍：500字;
		//		要求：请编写一、背景介绍中 第一段，分布式系统设计背景介绍字数在1500字以上`,
		//	MaxTokens: 3096,
		//	Stream:    true,
		//}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			stream, err := chatgpt.CreateCompletionStream(context.Background(), tt.args.req)
			if err != nil {
				t.Logf("ChatCompletionStream error: %v", err)
				return
			}
			defer stream.Close()

			var res strings.Builder
			for {
				response, err := stream.Recv()
				if errors.Is(err, io.EOF) {
					t.Log(res.Len())
					fmt.Println(res.String())
					return
				}
				if err != nil {
					t.Logf("Stream error: %v", err)
					return
				}
				res.WriteString(response.Choices[0].Text)
			}
		})
	}
}

func TestCreateCompletionStream2(t *testing.T) {
	type args struct {
		req openai.CompletionRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"1", args{req: openai.CompletionRequest{
			Model:     openai.GPT3TextDavinci003,
			Prompt:    "设计一份java毕业论文，要求8000字",
			MaxTokens: 3600,
			Stream:    true,
		}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//for i := 0; i < 1; i++ {
			//	stream, err := chatgpt.CreateCompletionStream(context.Background(), tt.args.req)
			//	if err != nil {
			//		t.Logf("ChatCompletionStream error: %v", err)
			//		break
			//	}
			//	defer stream.Close()
			//
			//	var res strings.Builder
			//	for {
			//		response, err := stream.Recv()
			//		if errors.Is(err, io.EOF) {
			//			t.Log(res.Len())
			//			t.Log(res.String())
			//			break
			//		}
			//		if err != nil {
			//			t.Logf("Stream error: %v", err)
			//			break
			//		}
			//		res.WriteString(response.Choices[0].Text)
			//	}
			//}
		})
	}
}

func TestName(t *testing.T) {

}
