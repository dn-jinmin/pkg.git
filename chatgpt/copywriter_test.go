/**
 * @Author: dn-jinmin
 * @File:  chatgpt_test
 * @Version: 1.0.0
 * @Date: 2023/4/20
 * @Description:
 */

package chatgpt

import (
	"testing"
)

func TestParseCopywriterPrompt(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    *CopywriterPrompt
		wantErr bool
	}{
		{
			"1", args{
				input: `
                                        主题: Java面向对象程序设计技术发展研究
                                        介绍: 本研究将讨论Java面向对象编程技术在当今软件开发中的重要地位，为其他技术的发展提供支持，有助于了解面向对象编程的优势和劣势，并介绍面向对象程序设计领域的发展情况。此外，还分析了Java中的程序设计概念，如封装、继承和多态，分析了现有的技术及其应用条件。最后，将介绍Java程序设计技术的优势，并提出一些建议和创新思路，以带动Java技术的进一步发展。
                                        总共约：8000字;
                                        可分为5部分:
                                        一、Java程序设计概述：1000字;
                                        二、面向对象编程技术：2000字;
                                        三、Java程序设计原理：2000字;
                                        四、Java程序设计应用：2000字;
                                        五、Java程序设计未来展望：1000字;
`,
			}, nil, false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseCopywriterPrompt(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseCopywriterPrompt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			DD(got)
			//if !reflect.DeepEqual(got, tt.want) {
			//	t.Errorf("ParseCopywriterPrompt() got = %v, want %v", got, tt.want)
			//}
		})
	}
}
