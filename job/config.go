/**
 * @author: dn-jinmin/dn-jinmin
 * @doc: 任务配置
 */

package job

import (
	"gitee.com/dn-jinmin/pkg/job/queue"
	"github.com/Shopify/sarama"
	"time"
)

const (
	NoResponse   sarama.RequiredAcks = 0
	WaitForLocal sarama.RequiredAcks = 1
	WaitForAll   sarama.RequiredAcks = -1
)

type Config struct {
	// 运维电话号码
	OpsMobile string `json:",optional"`
	// 运维邮箱
	OpsEmail string `json:",optional"`
	//RabbitSenderConf rabbitmq.RabbitSenderConf `json:",optional"`
	KqProducer queue.KqProducerConf `json:",optional"`
}
type KqProducer struct {
	Addrs         []string            `json:",optional"`
	Topic         string              `json:",optional"`
	Timeout       time.Duration       `json:",optional"`
	Ack           sarama.RequiredAcks `json:",optional"`
	RetryMax      int                 `json:",optional"`
	RetryBackoff  time.Duration       `json:",optional"`
	ReturnErrors  bool                `json:",optional"`
	ReturnSuccess bool                `json:",optional"`
}
