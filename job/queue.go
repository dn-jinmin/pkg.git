/**
 * @Author: dn-jinmin
 * @File:  job
 * @Version: 1.0.0
 * @Date: 2024/3/5
 * @Description:
 */

package job

type Queue interface {
	Send(exchange string, routeKey string, msg []byte) error
}
