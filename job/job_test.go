/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package job

import (
	"context"
	"github.com/zeromicro/go-queue/rabbitmq"
	"testing"
)

func getJobExec() *Job {
	return New(Config{
		OpsMobile:        "",
		OpsEmail:         "",
		RabbitSenderConf: rabbitmq.RabbitSenderConf{},
	})
}

func TestJob_ExecTask(t *testing.T) {

	type args struct {
		h    Handler
		task *Task
		opt  []CallOption
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"1", args{
			h: func(ctx context.Context) error {
				t.Log("测试执行")
				return nil
			},
			task: &Task{
				Desc: "测试",
			},
			opt: []CallOption{
				CallSetTask(&Task{
					Desc: "测试6666",
				}),
			},
		}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := getJobExec()
			if err := j.ExecTask(tt.args.h, tt.args.task, tt.args.opt...); (err != nil) != tt.wantErr {
				t.Errorf("ExecTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
