/**
 * @author: dn-jinmin/dn-jinmin
 * @doc: 任务示例，根据任务的类型处理投递到合适的队列机制中 | 专注多种任务处理
 * 专注在对任务的执行：基于任务执行中可能存在的出错、投递等需进行处理进行封装，当任务出错投递失败时可配置重试、超时熔断、失败投递任务、任务失败根据等级进行失败处理
 */

package job

import (
	"context"
	"encoding/json"
	"gitee.com/dn-jinmin/pkg/job/queue"
	"time"

	"gitee.com/dn-jinmin/pkg/breaker"
	"gitee.com/dn-jinmin/pkg/tool/logx"
	"gitee.com/dn-jinmin/pkg/xerr"

	"github.com/zeromicro/go-zero/core/stringx"
	"go.uber.org/zap"
)

type Handler func(ctx context.Context) error

type Job struct {
	rabbitmqSender Queue
	kqPusher       *queue.KafkaProducer
	cfg            Config
	brk            breaker.Breaker

	errHandlers *erroHandlers

	opt    options
	logger *zap.Logger
}

func New(cfg Config, opts ...Options) *Job {
	job := Job{
		opt:         newOption(opts),
		cfg:         cfg,
		errHandlers: newErrHandlers(cfg),
		logger:      logx.DefaultLogger(),
		brk:         breaker.NewBreaker(),
	}

	//// 初始化mq的发送方法
	//if cfg.RabbitSenderConf.Port != 0 {
	//	//job.rabbitmqSender = rabbitmq.MustNewSender(cfg.RabbitSenderConf)
	//}

	// 初始化kafka发送方法
	if len(cfg.KqProducer.Addrs) > 0 {
		job.kqPusher = queue.NewKafkaProducer(&cfg.KqProducer)
	}

	return &job
}

// 执行任务
func (j *Job) ExecTask(h Handler, task *Task, opt ...CallOption) error {
	return j.ExecTaskCtx(context.Background(), h, task, opt...)
}

// 执行任务
func (j *Job) ExecTaskCtx(ctx context.Context, h Handler, task *Task, opts ...CallOption) error {
	//if h == nil {
	//	return errors.New("未设置执行方法" + task.Desc)
	//}

	// 初始化执行设置
	callOpt := newCallOption(j.opt.callOption, opts...)
	if callOpt.task != nil {
		task = callOpt.task
	}
	// 执行任务
	if h != nil {
		if err := j.execTask(ctx, h, task, callOpt); err == nil || !callOpt.failedDelivery {
			// 没有异常 || 任务不需要投递
			return err
		}
	}
	// 验证队列默认执行设置
	j.queueDefaultRabbitmq(callOpt)
	// 投递任务到队列
	return j.sendQueue(ctx, callOpt.failedTaskQueue, task, callOpt)
}

func (j *Job) sendQueue(ctx context.Context, queue string, task *Task, call *callOption) error {
	d, err := json.Marshal(task)
	if err != nil {
		return err
	}

	// 根据类型选择执行
	switch call.queueType {
	case Kafka:
	case Rabbitmq:
		err = j.rabbitmqSender.Send(call.rabbitmqOption.exchange, queue, d)
	}
	return err
}

// 执行任务
func (j *Job) execTask(ctx context.Context, h Handler, task *Task, opt *callOption) error {
	task.Id = j.taskId(task.Id)
	// 执行实际程序
	err := j.retry(ctx, h, task, opt)
	if err == nil {
		return nil
	}

	task.Err = err.Error()

	// 错误处理
	return j.errHandlers.handler(task, opt, err)
}

// Retry
//
//	@Description: 任务重试
//	@receiver j
func (j *Job) retry(ctx context.Context, h Handler, task *Task, opt *callOption) error {
	// 判断是否设置超时
	_, ok := ctx.Deadline()
	if !ok {
		// no deadline so we create a new one
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, opt.timeout)
		defer cancel()
	}

	var herr error

	retries := opt.retry
	ch := make(chan error, retries+1)
	for i := 0; i < retries; i++ {

		go func() {
			task.Count++
			ch <- h(ctx)
		}()

		select {
		case herr = <-ch:
			if herr == nil {
				return nil
			}

			if opt.retryInterval > 0 {
				time.Sleep(opt.retryInterval)
			}
		case <-ctx.Done():
			return xerr.ErrorSysTimeout
		}
	}
	return herr
}

// 任务id
func (j *Job) taskId(id string) string {
	if id == "" {
		id = stringx.RandId()
	}
	return id
}

// 验证队列执行设置，默认为rabbitmq
func (j *Job) queueDefaultRabbitmq(callOpt *callOption) {
	if callOpt.queueType == 0 {
		callOpt.queueType = Rabbitmq
	}
}

// 验证队列执行设计，默认为kafka
func (j *Job) queueDefaultKafka(callOpt *callOption) {
	if callOpt.queueType == 0 {
		callOpt.queueType = Kafka
	}
}

// 执行失败处理
func (j *Job) failedOnErr(msg string, err error) {
	//logx.Debugf("【job task】msg:[%s] ; err:[%s]", msg, err.Error())
}
