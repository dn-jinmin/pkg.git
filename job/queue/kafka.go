/**
 * @Author: dn-jinmin
 * @File:  job
 * @Version: 1.0.0
 * @Date: 2023/3/30
 * @Description:
 */

package queue

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Shopify/sarama"
)

type KafkaProducer struct {
	producer sarama.SyncProducer
}

func NewKafkaProducer(config *KqProducerConf) *KafkaProducer {
	cfg := sarama.NewConfig()

	cfg.Producer.RequiredAcks = config.Ack

	if config.Timeout > 0 {
		cfg.Producer.Timeout = config.Timeout
	}

	if config.RetryMax > 0 {
		cfg.Producer.Retry.Max = config.RetryMax
	}
	if config.RetryBackoff > 0 {
		cfg.Producer.Retry.Backoff = config.RetryBackoff
	}
	if config.ReturnErrors {
		cfg.Producer.Return.Errors = config.ReturnErrors
	}

	if config.ReturnSuccess {
		cfg.Producer.Return.Successes = config.ReturnSuccess
	}

	client, err := sarama.NewSyncProducer(config.Addrs, cfg)
	if err != nil {
		panic(fmt.Sprintf("kafka producer new error %v, addrs %v", err, config.Addrs))
	}
	return &KafkaProducer{producer: client}
}

func (kp *KafkaProducer) Close() {
	kp.producer.Close()
}

func (kp *KafkaProducer) Send(topic string, data interface{}) error {

	if data == nil {
		return errors.New("无发送消息内容")
	}

	if topic == "" {
		return errors.New("w")
	}

	encode, err := kp.encode(data)
	if err != nil {
		return err
	}
	_, _, err = kp.producer.SendMessage(&sarama.ProducerMessage{
		Topic: topic,
		Value: encode,
	})
	return err
}

func (kp *KafkaProducer) encode(data interface{}) (sarama.Encoder, error) {
	switch data.(type) {
	case string:
		return sarama.StringEncoder(data.(string)), nil
	case []byte:
		return sarama.ByteEncoder(data.([]byte)), nil
	case uint, uint8, uint16, uint32, uint64, int, int8, int16, int32, int64:
		return sarama.StringEncoder(fmt.Sprintf("%d", data)), nil
	case float32, float64:
		return sarama.StringEncoder(fmt.Sprintf("%f", data)), nil
	default:
		d, err := json.Marshal(data)
		if err != nil {
			return nil, err
		}
		return sarama.ByteEncoder(d), nil
	}
}
