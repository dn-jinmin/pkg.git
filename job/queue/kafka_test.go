/**
 * @Author: dn-jinmin
 * @File:  queue_test
 * @Version: 1.0.0
 * @Date: 2023/3/30
 * @Description:
 */

package queue

import (
	"testing"
)

func TestKafkaProducer_Send(t *testing.T) {
	type args struct {
		kp    *KafkaProducer
		topic string
		data  interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"1", args{
				kp: NewKafkaProducer(&KqProducerConf{
					Addrs: []string{"192.168.145.10"},
				}),
				topic: "",
				data:  nil,
			}, false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.args.kp.Send(tt.args.topic, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Send() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
