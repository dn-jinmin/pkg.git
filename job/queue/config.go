/**
 * @author: dn-jinmin/dn-jinmin
 * @doc: 任务配置
 */

package queue

import (
	"github.com/Shopify/sarama"
	"time"
)

const (
	NoResponse   sarama.RequiredAcks = 0
	WaitForLocal sarama.RequiredAcks = 1
	WaitForAll   sarama.RequiredAcks = -1
)

type KqProducerConf struct {
	Addrs         []string            `json:",optional"`
	Timeout       time.Duration       `json:",optional"`
	Ack           sarama.RequiredAcks `json:",optional"`
	RetryMax      int                 `json:",optional"`
	RetryBackoff  time.Duration       `json:",optional"`
	ReturnErrors  bool                `json:",optional"`
	ReturnSuccess bool                `json:",optional"`
}
