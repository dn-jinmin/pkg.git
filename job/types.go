/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package job

import "fmt"

// Task
// @Description: 任务
type Task struct {
	// 任务编号：==》》 发送方生成
	Id string `json:"Id,optional"`
	// 处理方案：==》》 业务方选择
	Logic string `json:"Logic,optional"`
	// 任务描述：==》》 业务方描述
	Desc string `json:"Desc,optional"`
	// 执行次数
	Count int `json:"Count,optional"`
	// 重试次数
	Retry int `json:"Retry,optional"`
	// 重试间隔 默认 1
	RetryInterval int `json:"RetryInterval,optional"`
	// 任务失败警告级别
	// 1. S级立即发送短信
	// 2. A级多次发送邮件
	// 3. B级多次记录日志
	// 4. C级可以忽略
	TaskLevel int `json:"TaskLevel,optional"`
	// 重新入队
	FailedDelivery bool `json:"FailedDelivery,optional"`
	// tcp, kafka, rocketMQ, rabbitMQ, pulsarMQ
	ServerMode int `json:"ServerMode,optional"`
	// 任务数据：==》》 数据内容由生产者与消费者约定
	Data interface{} `json:"Data,optional"`
	// 错误
	Err string `json:"Err,optional"`
}

func (t *Task) ToString() string {
	return fmt.Sprintf("id = %s , logic = %s , desc = %s, count = %d, retry = %d, retryInterval = %d, "+
		"tasklevel = %d, failedDelivery = %v, serverMode = %d, data = %v, err = %s",
		t.Id, t.Logic, t.Desc, t.Count, t.Retry, t.RetryInterval, t.TaskLevel, t.FailedDelivery, t.ServerMode, t.Data, t.Err)
}
