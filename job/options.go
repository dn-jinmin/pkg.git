/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */

package job

import (
	"time"
)

type (
	Options func(opts *options)

	options struct {
		//kqPushOptions []kq.PushOption
		callOption
	}
)

// 初始化操作方法
func newOption(opts []Options) options {
	var opt options

	for _, f := range opts {
		f(&opt)
	}

	return opt
}

//func WithKqFlushInterval(interval time.Duration) Options {
//	return func(opts *options) {
//		opts.kqPushOptions = append(opts.kqPushOptions, kq.WithFlushInterval(interval))
//	}
//}
//func WithKqChunkSize(chunkSize int) Options {
//	return func(opts *options) {
//		opts.kqPushOptions = append(opts.kqPushOptions, kq.WithChunkSize(chunkSize))
//	}
//}

func WithCallQueue(queue int) Options {
	return func(opts *options) {
		opts.callOption.queueType = queue
	}
}

func WithCallFailedDelivery(delivery bool) Options {
	return func(opts *options) {
		opts.callOption.failedDelivery = delivery
	}
}

const (
	Rabbitmq = iota
	Kafka
	DefaultRetries       = 1
	DefaultRetryInterval = 1 * time.Second

	DefaultTimeout         = 1 * time.Second
	DefaultErrHandelrLevel = CELevel
)

type (
	CallOption func(opt *callOption)

	callOption struct {
		// 任务重试次数
		retry int
		// 任务重试间隔次数
		retryInterval time.Duration
		// 任务级别
		errHandlerLevel int
		// 失败处理
		errHandler ErrHandler
		// 队列类型
		queueType int
		// 请求超时,默认超时1s
		timeout time.Duration
		// 任务失败投递
		failedDelivery bool
		// 队列名
		failedTaskQueue string

		rabbitmqOption rabbitmqOption

		taskId string
		task   *Task
	}

	rabbitmqOption struct {
		exchange string
	}
)

func newCallOption(opt callOption, opts ...CallOption) *callOption {
	for _, f := range opts {
		f(&opt)
	}

	// 设置任务失败级别
	if opt.errHandlerLevel == 0 {
		opt.errHandlerLevel = DefaultErrHandelrLevel
	}
	// 如果为0不执行
	if opt.retry == 0 {
		opt.retry = DefaultRetries
	}

	if opt.timeout == 0 {
		opt.timeout = DefaultTimeout
	}
	return &opt
}

// 设置执行队列
func CallQueue(queue int) CallOption {
	return func(opt *callOption) {

		opt.queueType = queue
	}
}

// 设置rabbitmq exchange
func CallRabbitmqExchange(exchange string) CallOption {
	return func(opt *callOption) {
		opt.rabbitmqOption.exchange = exchange
	}
}

// 设置重试
func CallRetry(retry int, retryInterval time.Duration) CallOption {
	return func(opt *callOption) {
		opt.retryInterval = retryInterval * time.Second
		if opt.retryInterval == 0 {
			opt.retryInterval = DefaultRetryInterval
		}

		opt.retry = retry
		if opt.retry == 0 {
			opt.retry = DefaultRetries
		}
	}
}

// 设置任务级别
func CallErrHandlerLevel(level int) CallOption {
	return func(opt *callOption) {
		opt.errHandlerLevel = level
		if opt.errHandlerLevel == 0 {
			opt.errHandlerLevel = DefaultErrHandelrLevel
		}
	}
}

// 错误处理
func CallErrHandler(h ErrHandler) CallOption {
	return func(opt *callOption) {
		opt.errHandler = h
	}
}

// 设置请求超时
func CallTimeout(timeout time.Duration) CallOption {
	return func(opt *callOption) {
		opt.timeout = timeout * time.Second
	}
}

// 设置任务失败投递
func CallFailedDelivery(devlivery bool) CallOption {
	return func(opt *callOption) {
		opt.failedDelivery = devlivery
	}
}

func CallSetTask(task *Task) CallOption {
	return func(opt *callOption) {
		opt.task = task
	}
}
