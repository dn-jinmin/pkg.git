/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package job

import (
	"fmt"
	"time"
)

// 任务失败警告级别
// 1. S级立即发送短信 下面的都做
// 2. A级多次发送邮件
// 3. B级多次记录日志 -- 默认级别
// 4. C级可以忽略

const (
	SELevel = iota + 1
	AELevel
	BELevel // 默认级别
	CELevel
)

var levelText = map[int]string{
	CELevel: "C级别 不做处理",
	BELevel: "B级别 输出日志",
	AELevel: "A级别 输出日志, 发送邮件",
	SELevel: "S级别 输出日志, 发送邮件, 发送短信",
}

type ErrHandlerInfo struct {
	// 任务信息
	*Task
	// 信息任务等级
	Level int
	// 信息创建时间
	Create_at time.Time
	// 信息有效时间
	ActiveTime time.Time
	// 通知
	Inform string
}

// 消息是否过期
func (info *ErrHandlerInfo) Overdue() bool {
	return time.Now().After(info.ActiveTime)
}

// 记录任务发送
type erroHandlers struct {
	errHandlerS     map[string]*ErrHandlerInfo
	errHandlerLevel map[int]ErrHandler
	config          Config
}

func newErrHandlers(c Config) *erroHandlers {
	return &erroHandlers{
		errHandlerS: map[string]*ErrHandlerInfo{},
		errHandlerLevel: map[int]ErrHandler{
			CELevel: ErrIgnore,
			BELevel: ErrWriteLogs,
			AELevel: ErrSendEmail,
			SELevel: ErrSendSms,
		},
		config: c,
	}
}

func (e erroHandlers) handler(task *Task, opt *callOption, err error) error {
	if err == nil {
		return nil
	}

	// 获取之前处理信息
	// 1. 如果存在往期错误信息记录则直接使用，没有就生成新的错误信息
	// 2. 校验任务是否过期

	handerInfo, ok := e.errHandlerS[task.Id]
	if ok && !handerInfo.Overdue() {
		// 未过期不发生
		return nil
	} else if !ok {
		now := time.Now()
		handerInfo = &ErrHandlerInfo{
			Task:       task,
			Level:      opt.errHandlerLevel,
			Create_at:  now,
			ActiveTime: now.AddDate(0, 0, 1),
			Inform:     "",
		}
	}

	if opt.errHandler != nil {
		// 自定义处理机制
		handerInfo, err = opt.errHandler(task, handerInfo, err)
	} else {
		// 获取依据等级处理方法
		handler, ok := e.errHandlerLevel[opt.errHandlerLevel]
		if !ok {
			// 获取不到失败警告处理，忽略
			return nil
		}
		handerInfo, err = handler(task, handerInfo, err)
	}

	if handerInfo != nil && handerInfo.Inform != "" {
		e.errHandlerS[task.Id] = handerInfo
	}

	return err
}

// 警告处理
type ErrHandler func(*Task, *ErrHandlerInfo, error) (*ErrHandlerInfo, error)

// 忽略
func ErrIgnore(task *Task, info *ErrHandlerInfo, err error) (*ErrHandlerInfo, error) {
	fmt.Println("===》》》》 忽略")
	return nil, err
}

// 写入日志
func ErrWriteLogs(task *Task, info *ErrHandlerInfo, err error) (*ErrHandlerInfo, error) {

	if info.Inform == "" || info.Overdue() {
		// todo : log
	}
	info.Inform = "write log"

	return info, err
}

func ErrSendEmail(task *Task, info *ErrHandlerInfo, err error) (*ErrHandlerInfo, error) {
	if info.Inform == "" || info.Overdue() {
		ErrWriteLogs(task, info, err)
	}
	info.Inform = "write log and send email"
	return info, err
}

// 发送短信
func ErrSendSms(task *Task, info *ErrHandlerInfo, err error) (*ErrHandlerInfo, error) {
	if info.Inform == "" || info.Overdue() {
		ErrSendEmail(task, info, err)
	}
	info.Inform = "write log and send email and send sms"
	return info, err
}
