/*
 * @auther: dn-jinmin/dn-jinmin
 * @desc:
 */

package sql

import (
	"gitee.com/dn-jinmin/pkg/xerr"
	"database/sql"
)

func ModelAddResultValidationVerifyIdResId(res sql.Result,err error) (id int64,xe error, err2 error) {
	if err != nil {
		return 0, xerr.ErrorDbErr, err
	}
	id, err = res.LastInsertId()
	if err != nil {
		return 0, xerr.ErrorDbErr, err
	} else if id == 0 {
		return 0, xerr.ErrorDataAdditionException, err
	}

	return id, nil, err
}

func ModelAddResultValidationVerifyRows(res sql.Result,err error) (xe error, err2 error) {
	if err != nil {
		return xerr.ErrorDbErr, err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		return  xerr.ErrorDbErr, err
	} else if rows == 0 {
		return  xerr.ErrorDataAdditionException, err
	}

	return nil, err
}
