/**
 * @author: dn-jinmin/dn-jinmin
 * @doc: zinx tcp client
 */
package netx

import (
	"encoding/json"
	"io"
	"net"

	"github.com/aceld/zinx/ziface"
	"github.com/aceld/zinx/znet"
)

const DefaultTcpServiceId = 0

type Message struct {
	*znet.Message
}

// TcpClient
// @Description: tcp客户端
type TcpClient struct {
	conn net.Conn
	dp   ziface.IDataPack
}

func NewTcpClient(addr string) *TcpClient {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		panic(err)
	}

	return &TcpClient{
		conn: conn,
		dp:   znet.NewDataPack(),
	}
}

// Send
//
//	@Description: 发送信息
//	@receiver c
//	@param id 消息id
//	@param data 消息内容
//	@return error
func (c *TcpClient) Send(id uint32, v interface{}) error {

	data, err := marshal(v)
	if err != nil {
		return err
	}

	msg, err := c.dp.Pack(znet.NewMsgPackage(id, data))
	if err != nil {
		return err
	}

	_, err = c.conn.Write(msg)
	if err != nil {
		return err
	}
	return nil
}

// Rece
//
//	@Description: 接收信息
//	@receiver c
//	@param v
//	@return *Message 消息详情
//	@return error 错误
func (c *TcpClient) Rece(v interface{}) (*Message, error) {
	headData := make([]byte, c.dp.GetHeadLen())
	// ReadFull 会把msg填充满为止
	// 获取消息包头
	_, err := io.ReadFull(c.conn, headData)
	if err != nil {
		return nil, err
	}

	// 解包获取到msg
	msgHead, err := c.dp.Unpack(headData)
	if err != nil {
		return nil, err
	}

	if msgHead.GetDataLen() > 0 {
		// 存在消息
		// msg 是有data数据的，需要再次读取data数据
		var msg Message
		msg.Message = msgHead.(*znet.Message)
		msg.Data = make([]byte, msg.GetDataLen())

		// 根据dataLen从io中读取字节流
		_, err = io.ReadFull(c.conn, msg.Data)
		if err != nil {
			return nil, err
		}

		unmarshal(msg.Data, v)

		return &msg, nil
	}
	return nil, err
}

func marshal(v interface{}) (msg []byte, err error) {
	if v == nil {
		return nil, nil
	}

	switch data := v.(type) {
	case string:
		return []byte(data), nil
	case []byte:
		return data, nil
	default:
		return json.Marshal(v)
	}
}

func unmarshal(data []byte, v interface{}) error {
	if len(data) == 0 || v == nil {
		return nil
	}

	switch v.(type) {
	case string:
		v = string(data)
	case []byte:
		v = data
	default:
		return json.Unmarshal(data, v)
	}
	return nil
}
