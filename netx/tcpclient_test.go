/**
 * @author: dn-jinmin/dn-jinmin
 * @doc:
 */
package netx

import "testing"

func TestTcpClient(t *testing.T) {
	client := NewTcpClient("127.0.0.1:9010")

	type Content struct {
		Data string
		Code int
	}

	// 发送
	err := client.Send(DefaultTcpServiceId, &Content{
		Data: "hello world zinx server",
		Code: 200,
	})
	if err != nil {
		t.Logf("tcp client send err := %s", err.Error())
	}

	// 接收
	var c Content
	msg, err := client.Rece(&c)
	if err != nil {
		t.Logf("tcp client rece err := %s", err.Error())
	}

	t.Logf("c := %+v, msg := %+v", c, msg)

}
